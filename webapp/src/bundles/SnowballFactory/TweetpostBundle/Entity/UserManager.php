<?php

namespace SnowballFactory\TweetpostBundle\Entity;

use Symfony\Component\DependencyInjection\Container;

use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\EntityManager;

use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\CanonicalizerInterface;

use FOS\UserBundle\Entity\UserManager as BaseUserManager;

class UserManager extends BaseUserManager implements UserProviderInterface, UserCheckerInterface
{
    public function supportsClass($class)
    {
        return $class === $this->getClass();
    }

    public function findUserByUsername($username)
    {
        return $this->findUserBy(array('username' => $username));
    }

    public function loadUserByUsername($username)
    {
        $user = $this->findUserByUsername($username);

        if (empty($user)) {
            throw new UsernameNotFoundException('The user is not authenticated on facebook');
        }

        return $user;
    }

    /**
     * Refresh a user
     *
     * @param UserInterface $user
     */
    public function loadUser(BaseUserInterface $user)
    {
        if (!$this->supportsClass(get_class($user)) || !$user->getUsername()) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function checkPreAuth(BaseUserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException('User is not supported.');
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function checkPostAuth(BaseUserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException('User is not supported.');
        }
    }
}
