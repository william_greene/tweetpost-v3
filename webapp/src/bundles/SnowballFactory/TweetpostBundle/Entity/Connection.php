<?php

namespace SnowballFactory\TweetpostBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Constraints as Assert;

use \DateTime;

use SnowballFactory\TweetpostBundle\Entity\User;
use SnowballFactory\TweetpostBundle\Entity\TwitterUser;
use SnowballFactory\TweetpostBundle\Entity\Tweet;


/**
 * @ORM\Entity(repositoryClass="SnowballFactory\TweetpostBundle\Repository\ConnectionRepository")
 * @ORM\Table(
 *            name="connections",
 *            uniqueConstraints={@ORM\UniqueConstraint(name="connection_unique",columns={"user_id", "twitter_id", "facebook_id", "type"})}
 *           )
 * @ORM\HasLifecycleCallbacks
 */
class Connection
{
    /**
     * @var bigint $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer", name="id", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="connections", cascade={"persist"})
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="TwitterUser", inversedBy="connections", cascade={"persist"})
     */
    protected $twitter;

    /**
     * @ORM\Column(type="bigint", name="facebook_id", nullable=false)
     */
    protected $facebookId;

    /**
     * @ORM\Column(type="string", name="name", length=250, nullable=false)
     *
     * @Assert\NotBlank()
     * @Assert\MinLength(1)
     * @Assert\MaxLength(250)
     */
    protected $name;

    /**
     * @ORM\Column(type="boolean", name="active", nullable=false)
     *
     */
    protected $active;

    /**
     * @ORM\Column(type="string", name="type", length=250, nullable=true)
     */
    protected $type;

    /**
     * @ORM\Column(type="string", name="access_token", length=250, nullable=true)
     */
    protected $accessToken;

    /**
     * @ORM\Column(type="boolean", name="filter_hashtag", nullable=false)
     *
     */
    protected $filterHashtag;

    /**
     * @ORM\Column(type="boolean", name="convert_names", nullable=false)
     *
     */
    protected $convertNames;

    /**
     * @ORM\Column(type="boolean", name="post_links", nullable=false)
     *
     */
    protected $postLinks;

    /**
     * @ORM\Column(type="boolean", name="awesm_tracking", nullable=false)
     *
     */
    protected $awesmTracking;

    /**
     * @ORM\Column(type="string", name="awesm_api_key", length=250, nullable=true)
     *
     */
    protected $awesmApiKey;

    /**
     * @ORM\Column(type="boolean", name="action_link", nullable=false)
     *
     */
    protected $actionLink;

    /**
     * @ORM\Column(type="boolean", name="disable_retweet", nullable=false)
     *
     */
    protected $disableRetweet;

    /**
     * @ORM\Column(type="array", name="blacklist", nullable=true)
     *
     */
    protected $blacklist;

    /**
     * @ORM\Column(type="integer", name="recent_failures", nullable=true)
     */
    protected $recentFailures;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     */
    protected $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    protected $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="Tweet", mappedBy="connection")
     */
    protected $tweets;

    public function __construct()
    {
        $this->tweets = new ArrayCollection();
        $this->posts  = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param integer $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get user
     *
     * @return integer $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set twitter
     *
     * @param integer $twitter
     */
    public function setTwitter(TwitterUser $twitter)
    {
        $this->twitter = $twitter;
    }

    /**
     * Get twitter
     *
     * @return integer $twitter
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set facebookId
     *
     * @param integer $facebookId
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
    }

    /**
     * Get facebookId
     *
     * @return integer $facebookId
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set active
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return boolean $active
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set accessToken
     *
     * @param string $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * Get accessToken
     *
     * @return string $accessToken
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * Set filterHashtag
     *
     * @param boolean $filterHashtag
     */
    public function setFilterHashtag($filterHashtag)
    {
        $this->filterHashtag = $filterHashtag;
    }

    /**
     * Get filterHashtag
     *
     * @return boolean $filterHashtag
     */
    public function getFilterHashtag()
    {
        return $this->filterHashtag;
    }

    /**
     * Set convertNames
     *
     * @param boolean $convertNames
     */
    public function setConvertNames($convertNames)
    {
        $this->convertNames = $convertNames;
    }

    /**
     * Get convertNames
     *
     * @return boolean $convertNames
     */
    public function getConvertNames()
    {
        return $this->convertNames;
    }

    /**
     * Set postLinks
     *
     * @param boolean $postLinks
     */
    public function setPostLinks($postLinks)
    {
        $this->postLinks = $postLinks;
    }

    /**
     * Get postLinks
     *
     * @return boolean $postLinks
     */
    public function getPostLinks()
    {
        return $this->postLinks;
    }

    /**
     * Set awesmTracking
     *
     * @param boolean $awesmTracking
     */
    public function setAwesmTracking($awesmTracking)
    {
        $this->awesmTracking = $awesmTracking;
    }

    /**
     * Get awesmTracking
     *
     * @return boolean $awesmTracking
     */
    public function getAwesmTracking()
    {
        return $this->awesmTracking;
    }

    /**
     * Set awesmApiKey
     *
     * @param string $awesmApiKey
     */
    public function setAwesmApiKey($awesmApiKey)
    {
        $this->awesmApiKey = $awesmApiKey;
    }

    /**
     * Get awesmApiKey
     *
     * @return string $awesmApiKey
     */
    public function getAwesmApiKey()
    {
        return $this->awesmApiKey;
    }

    /**
     * Set actionLink
     *
     * @param boolean $actionLink
     */
    public function setActionLink($actionLink)
    {
        $this->actionLink = $actionLink;
    }

    /**
     * Get actionLink
     *
     * @return boolean $actionLink
     */
    public function getActionLink()
    {
        return $this->actionLink;
    }

    /**
     * Set disableRetweet
     *
     * @param boolean $disableRetweet
     */
    public function setDisableRetweet($disableRetweet)
    {
        $this->disableRetweet = $disableRetweet;
    }

    /**
     * Get disableRetweet
     *
     * @return boolean $disableRetweet
     */
    public function getDisableRetweet()
    {
        return $this->disableRetweet;
    }

    /**
     * Set blacklist
     *
     * @param array $blacklist
     */
    public function setBlacklist(array $blacklist)
    {
        $this->blacklist = $blacklist;
    }

    /**
     * Get blacklist
     *
     * @return array $blacklist
     */
    public function getBlacklist()
    {
        return $this->blacklist;
    }

    /**
     * Set recentFailures
     *
     * @param integer $recentFailures
     */
    public function setRecentFailures($recentFailures)
    {
        $this->recentFailures = $recentFailures;
    }

    /**
     * Get recentFailures
     *
     * @return integer $recentFailures
     */
    public function getRecentFailures()
    {
        return $this->recentFailures;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime $updatedAt
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add tweets
     *
     * @param Tweet $tweet
     */
    public function addTweet(Tweet $tweet)
    {
        $this->tweets[] = $tweet;
    }

    /**
     * Get tweets
     *
     * @return Doctrine\Common\Collections\Collection $tweets
     */
    public function getTweets()
    {
        return $this->tweets;
    }

}
