<?php

namespace SnowballFactory\TweetpostBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Constraints as Assert;

use \DateTime;

use SnowballFactory\TweetpostBundle\Entity\Connection;


/**
 * @ORM\Entity(repositoryClass="SnowballFactory\TweetpostBundle\Repository\TweetRepository")
 * @ORM\Table(
 *            name="tweets",
 *            indexes={@ORM\Index(name="connection_status_idx", columns={"connection_id", "status"})},
 *            uniqueConstraints={@ORM\UniqueConstraint(name="connection_tweet_unique",columns={"connection_id", "tweet_id"})}
 *           )
 * @ORM\HasLifecycleCallbacks
 */
class Tweet
{
    /**
     * @var bigint $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer", name="id", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Assert\NotBlank()
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Connection", inversedBy="tweets")
     *
     * @Assert\Type(type="SnowballFactory\TweetpostBundle\Entity\Connection")
     * @Assert\NotBlank()
     */
    protected $connection;

    /**
     * @ORM\Column(type="bigint", name="tweet_id", nullable=false)
     *
     * @Assert\NotBlank()
     */
    protected $tweetId;

    /**
     * @ORM\Column(type="bigint", name="tweet_user_id", nullable=true)
     *
     * @Assert\NotBlank()
     */
    protected $tweetUserId;

    /**
     * @ORM\Column(type="string", name="facebook_post_id", length=200, nullable=false)
     *
     * @Assert\NotBlank()
     */
    protected $facebookPostId;

    /**
     * @ORM\Column(type="bigint", name="facebook_user_id", nullable=true)
     *
     * @Assert\NotBlank()
     */
    protected $facebookUserId;

    /**
     * @ORM\Column(type="string", name="status", length=200, nullable=true)
     *
     * @Assert\NotBlank()
     */
    protected $status;

    /**
     * @ORM\Column(type="array", name="data", nullable=true)
     *
     * @Assert\NotBlank()
     */
    protected $data;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     *
     * @Assert\NotBlank()
     */
    protected $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     *
     * @Assert\NotBlank()
     */
    protected $updatedAt;


    public function __toString()
    {
        return $this->getStatus();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        $this->updatedAt = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set connection
     *
     * @param integer $connection
     */
    public function setConnection(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get connection
     *
     * @return integer $connection
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Set data
     *
     * @param integer $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    /**
     * Get data
     *
     * @return integer $data
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set tweetId
     *
     * @param integer $tweetId
     */
    public function setTweetId($tweetId)
    {
        $this->tweetId = $tweetId;
    }

    /**
     * Get tweetId
     *
     * @return integer $tweetId
     */
    public function getTweetId()
    {
        return $this->tweetId;
    }

    /**
     * Set tweetUserId
     *
     * @param integer $tweetUserId
     */
    public function setTweetUserId($tweetUserId)
    {
        $this->tweetUserId = $tweetUserId;
    }

    /**
     * Get tweetUserId
     *
     * @return integer $tweetUserId
     */
    public function getTweetUserId()
    {
        return $this->tweetUserId;
    }

    /**
     * Set facebookPostId
     *
     * @param integer $facebookPostId
     */
    public function setFacebookPostId($facebookPostId)
    {
        $this->facebookPostId = $facebookPostId;
    }

    /**
     * Get facebookPostId
     *
     * @return integer $facebookPostId
     */
    public function getFacebookPostId()
    {
        return $this->facebookPostId;
    }

    /**
     * Set facebookUserId
     *
     * @param integer $facebookUserId
     */
    public function setFacebookUserId($facebookUserId)
    {
        $this->facebookUserId = $facebookUserId;
    }

    /**
     * Get facebookUserId
     *
     * @return integer $facebookUserId
     */
    public function getFacebookUserId()
    {
        return $this->facebookUserId;
    }

    /**
     * Set status
     *
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime $updatedAt
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}