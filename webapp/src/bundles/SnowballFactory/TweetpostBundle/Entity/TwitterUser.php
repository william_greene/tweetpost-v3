<?php

namespace SnowballFactory\TweetpostBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Constraints as Assert;

use \DateTime;

use SnowballFactory\TweetpostBundle\Entity\User;
use SnowballFactory\TweetpostBundle\Entity\Connection;


/**
 * @ORM\Entity(repositoryClass="SnowballFactory\TweetpostBundle\Repository\TwitterUserRepository")
 * @ORM\Table(
 *            name="twitter_users",
 *            indexes={@ORM\Index(name="twitter_user_idx", columns={"user_id", "twitter_id"})},
 *            uniqueConstraints={@ORM\UniqueConstraint(name="twitter_user_unique",columns={"user_id", "twitter_id"})}
 *           )
 * @ORM\HasLifecycleCallbacks
 */
class TwitterUser
{
    /**
     * @var bigint $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer", name="id", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Assert\NotBlank()
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="twitterUsers")
     *
     * @Assert\Type(type="SnowballFactory\TweetpostBundle\Entity\User")
     * @Assert\NotBlank()
     */
    protected $user;

    /**
     * @ORM\Column(type="integer", name="twitter_id", nullable=false)
     */
    protected $twitterId;

    /**
     * @ORM\Column(type="string", name="username", length=64, nullable=true)
     */
    protected $username;

    /**
     * @ORM\Column(type="boolean", name="private", nullable=false)
     */
    protected $private;

    /**
     * @ORM\Column(type="string", name="name", length=200, nullable=false)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", name="picture", length=250, nullable=false)
     */
    protected $picture;

    /**
     * @ORM\Column(type="integer", name="last_tweet_id", length=250, nullable=true)
     */
    protected $lastTweetId;

    /**
     * @ORM\Column(type="string", name="access_token", length=250, nullable=true)
     */
    protected $accessToken;

    /**
     * @ORM\Column(type="string", name="access_token_secret", length=250, nullable=true)
     */
    protected $accessTokenSecret;

    /**
     * @ORM\Column(type="boolean", name="polling", nullable=false)
     */
    protected $polling;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     *
     * @Assert\NotBlank()
     */
    protected $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     *
     * @Assert\NotBlank()
     */
    protected $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="Connection", mappedBy="twitter")
     */
    protected $connections;

    public function __construct()
    {
        $this->connections  = new ArrayCollection();
    }

    public function __toString()
    {
        return '@'.$this->getUsername();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        $this->updatedAt = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param integer $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get user
     *
     * @return integer $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set twitterId
     *
     * @param integer $twitterId
     */
    public function setTwitterId($twitterId)
    {
        $this->twitterId = $twitterId;
    }

    /**
     * Get twitterId
     *
     * @return integer $twitterId
     */
    public function getTwitterId()
    {
        return $this->twitterId;
    }

    /**
     * Set username
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Get username
     *
     * @return string $username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set private
     *
     * @param boolean $private
     */
    public function setPrivate($private)
    {
        $this->private = $private;
    }

    /**
     * Get private
     *
     * @return boolean $private
     */
    public function getPrivate()
    {
        return $this->private;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set picture
     *
     * @param string $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * Get picture
     *
     * @return string $picture
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Set lastTweetId
     *
     * @param integer $lastTweetId
     */
    public function setLastTweetId($lastTweetId)
    {
        $this->lastTweetId = $lastTweetId;
    }

    /**
     * Get lastTweetId
     *
     * @return integer $lastTweetId
     */
    public function getLastTweetId()
    {
        return $this->lastTweetId;
    }

    /**
     * Set accessToken
     *
     * @param string $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * Get accessToken
     *
     * @return string $accessToken
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * Set accessTokenSecret
     *
     * @param string $accessTokenSecret
     */
    public function setAccessTokenSecret($accessTokenSecret)
    {
        $this->accessTokenSecret = $accessTokenSecret;
    }

    /**
     * Get accessTokenSecret
     *
     * @return string $accessTokenSecret
     */
    public function getAccessTokenSecret()
    {
        return $this->accessTokenSecret;
    }

    /**
     * Set polling
     *
     * @param boolean $polling
     */
    public function setPolling($polling)
    {
        $this->polling = $polling;
    }

    /**
     * Get polling
     *
     * @return boolean $polling
     */
    public function getPolling()
    {
        return $this->polling;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime $updatedAt
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add connections
     *
     * @param Connection $connection
     */
    public function addConnection(Connection $connection)
    {
        $this->connections[] = $connection;
    }

    /**
     * Get connections
     *
     * @return Doctrine\Common\Collections\Collection $connections
     */
    public function getConnections()
    {
        return $this->connections;
    }

}