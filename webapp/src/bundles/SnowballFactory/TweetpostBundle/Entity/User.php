<?php

namespace SnowballFactory\TweetpostBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface as SecurityUserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

use \DateTime;

use FOS\UserBundle\Model\UserInterface;

use SnowballFactory\TweetpostBundle\Entity\TwitterUser;
use SnowballFactory\TweetpostBundle\Entity\Connection;


/**
 * @ORM\Entity(repositoryClass="SnowballFactory\TweetpostBundle\Repository\UserRepository")
 * @ORM\Table(
 *            name="users",
 *            indexes={@ORM\Index(name="facebook_idx", columns={"facebook_id"})},
 *            uniqueConstraints={@ORM\UniqueConstraint(name="facebook_unique", columns={"facebook_id", "facebook_access_token"})}
 *           )
 * @ORM\HasLifecycleCallbacks
 */
class User implements UserInterface, EquatableInterface
{
    const ROLE_DEFAULT    = 'ROLE_USER';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    /**
     * @var bigint $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer", name="id", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Assert\NotBlank()
     */
    protected $id;

    /**
     * @var string $username
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    protected $username;

    /**
     * @var string $usernameCanonical
     *
     * @ORM\Column(name="username_canonical", type="string", length=255, unique=true)
     */
    protected $usernameCanonical;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    protected $email;

    /**
     * @var string $emailCanonical
     *
     * @ORM\Column(name="email_canonical", type="string", length=255, unique=true)
     */
    protected $emailCanonical;

    /**
     * @var boolean $enabled
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled;

    /**
     * @var string $algorithm
     *
     * @ORM\Column(name="algorithm", type="string", nullable=true)
     */
    protected $algorithm;

    /**
     * @var string $salt
     *
     * @ORM\Column(name="salt", type="string")
     */
    protected $salt;

    /**
     * @var string $password
     *
     * @ORM\Column(name="password", type="string")
     */
    protected $password;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     */
    protected $plainPassword;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @var datetime $lastLogin
     *
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    protected $lastLogin;

    /**
     * @var boolean $locked
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    protected $locked;

    /**
     * @var boolean $expired
     *
     * @ORM\Column(name="expired", type="boolean")
     */
    protected $expired;

    /**
     * @var datetime $expiresAt
     *
     * @ORM\Column(name="expires_at", type="datetime", nullable=true)
     */
    protected $expiresAt;

    /**
     * @var string $confirmationToken
     *
     * @ORM\Column(name="confirmation_token", type="string", nullable=true)
     */
    protected $confirmationToken;

    /**
     * @var datetime $passwordRequestedAt
     *
     * @ORM\Column(name="password_requested_at", type="datetime", nullable=true)
     */
    protected $passwordRequestedAt;

    /**
     * @var array $roles
     *
     * @ORM\Column(name="roles", type="array")
     */
    protected $roles;

    /**
     * @var boolean $credentialsExpired
     *
     * @ORM\Column(name="credentials_expired", type="boolean")
     */
    protected $credentialsExpired;

    /**
     * @var datetime $credentialsExpireAt
     *
     * @ORM\Column(name="credentials_expire_at", type="datetime", nullable=true)
     */
    protected $credentialsExpireAt;

    /**
     * @ORM\Column(type="string", name="first_name", length=64, nullable=false)
     *
     * @Assert\NotBlank()
     * @Assert\MinLength(2)
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string", name="last_name", length=64, nullable=false)
     *
     * @Assert\NotBlank()
     * @Assert\MinLength(2)
     */
    protected $lastName;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    protected $locale;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    protected $timezone;

    /**
     * @ORM\Column(type="bigint", name="facebook_id", nullable=false)
     *
     * @Assert\NotBlank()
     */
    protected $facebookId;

    /**
     * @ORM\Column(type="string", name="facebook_username", length=128, nullable=false)
     *
     * @Assert\NotBlank()
     */
    protected $facebookUsername;

    /**
     * @ORM\Column(type="string", name="facebook_access_token", length=250, nullable=true)
     *
     * @Assert\NotBlank()
     */
    protected $facebookAccessToken;

    /**
     * @ORM\OneToMany(targetEntity="TwitterUser", mappedBy="user", cascade={"persist", "remove"})
     */
    protected $twitterUsers;

    /**
     * @ORM\OneToMany(targetEntity="Connection", mappedBy="user", cascade={"persist", "remove"})
     */
    protected $connections;

    public function __construct()
    {
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $this->generateConfirmationToken();

        $this->enabled = false;
        $this->locked  = false;
        $this->expired = false;

        $this->roles   = array();
        $this->credentialsExpired = false;

        $this->twitterUsers = new ArrayCollection();
        $this->connections  = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        $this->updatedAt = new \DateTime();
    }

    public function addRole($role)
    {
        $role = strtoupper($role);
        if ($role === self::ROLE_DEFAULT) {
            return;
        }

        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }
    }

    public function isEqualTo(SecurityUserInterface $user)
    {
      return $this->equals($user);
    }

    /**
     * Implementation of SecurityUserInterface.
     *
     * @param SecurityUserInterface $account
     * @return boolean
     */
    public function equals(SecurityUserInterface $user)
    {
        if (!$user instanceof User) {
            return false;
        }

        if ($this->password !== $user->getPassword()) {
            return false;
        }
        if ($this->getSalt() !== $user->getSalt()) {
            return false;
        }
        if ($this->usernameCanonical !== $user->getUsernameCanonical()) {
            return false;
        }
        if ($this->isAccountNonExpired() !== $user->isAccountNonExpired()) {
            return false;
        }
        if (!$this->locked !== $user->isAccountNonLocked()) {
            return false;
        }
        if ($this->isCredentialsNonExpired() !== $user->isCredentialsNonExpired()) {
            return false;
        }
        if ($this->enabled !== $user->isEnabled()) {
            return false;
        }

        return true;
    }

    /**
     * Removes sensitive data from the user.
     *
     * Implements SecurityUserInterface
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Gets the canonical username in search and sort queries.
     *
     * @return string
     */
    public function getUsernameCanonical()
    {
        return $this->usernameCanonical;
    }

    /**
     * Implementation of SecurityUserInterface
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    public function getAlgorithm()
    {
        return $this->algorithm;
    }

    /**
     * Gets email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Gets the canonical email in search and sort queries.
     *
     * @return string
     */
    public function getEmailCanonical()
    {
        return $this->emailCanonical;
    }

    /**
     * Gets the encrypted password.
     *
     * Implements SecurityUserInterface
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return \DateTime
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Gets the confirmation token.
     *
     * @return string
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * Returns the user roles
     *
     * Implements SecurityUserInterface
     *
     * @return array The roles
     **/
    public function getRoles()
    {
        $roles = $this->roles;

        $roles[] = self::ROLE_DEFAULT;

        return array_unique($roles);
    }

    /**
     * Never use this to check if this user has access to anything!
     *
     * Use the SecurityContext, or an implementation of AccessDecisionManager
     * instead, e.g.
     *
     *         $securityContext->isGranted('ROLE_USER');
     *
     * @param string $role
     * @return Boolean
     */
    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    /**
     * Checks whether the user's account has expired.
     *
     * Implements AdvancedUserInterface
     *
     * @return Boolean true if the user's account is non expired, false otherwise
     */
    public function isAccountNonExpired()
    {
        if (true === $this->expired) {
            return false;
        }

        if (null !== $this->expiresAt && $this->expiresAt->getTimestamp() < time()) {
            return false;
        }

        return true;
    }

    /**
     * Checks whether the user is locked.
     *
     * Implements AdvancedUserInterface
     *
     * @return Boolean true if the user is not locked, false otherwise
     */
    public function isAccountNonLocked()
    {
        return !$this->locked;
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Implements AdvancedUserInterface
     *
     * @return Boolean true if the user's credentials are non expired, false otherwise
     */
    public function isCredentialsNonExpired()
    {
        if (true === $this->credentialsExpired) {
            return false;
        }

        if (null !== $this->credentialsExpireAt && $this->credentialsExpireAt->getTimestamp() < time()) {
            return false;
        }

        return true;
    }

    public function isCredentialsExpired()
    {
        return !$this->isCredentialsNonExpired();
    }

    /**
     * Checks whether the user is enabled.
     *
     * Implements AdvancedUserInterface
     *
     * @return Boolean true if the user is enabled, false otherwise
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    public function isExpired()
    {
        return !$this->isAccountNonExpired();
    }

    public function isLocked()
    {
        return $this->locked;
    }

    /**
     * Tells if the the given user has the super admin role.
     *
     * @return Boolean
     */
    public function isSuperAdmin()
    {
       return $this->hasRole(self::ROLE_SUPER_ADMIN);
    }

    /**
     * Tells if the the given user is this user.
     *
     * Useful when not hydrating all fields.
     *
     * @param UserInterface $user
     * @return Boolean
     */
    public function isUser(UserInterface $user = null)
    {
        return null !== $user && $this->getId() === $user->getId();
    }

    public function incrementCreatedAt()
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        $this->updatedAt = new \DateTime();
    }

    public function incrementUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    public function removeRole($role)
    {
        if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }
    }

    /**
     * Sets the username.
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Sets the canonical username.
     *
     * @param string $usernameCanonical
     */
    public function setUsernameCanonical($usernameCanonical)
    {
        $this->usernameCanonical = $usernameCanonical;
    }

    public function setAlgorithm($algorithm)
    {
        $this->algorithm = $algorithm;
    }

    public function setCredentialsExpireAt(\DateTime $date)
    {
        $this->credentialsExpireAt = $date;
    }

    public function setCredentialsExpired($boolean)
    {
        $this->credentialsExpired = $boolean;
    }

    /**
     * Sets the email.
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Set the canonical email.
     *
     * @param string $emailCanonical
     */
    public function setEmailCanonical($emailCanonical)
    {
        $this->emailCanonical = $emailCanonical;
    }

    /**
     * @param Boolean $boolean
     */
    public function setEnabled($boolean)
    {
        $this->enabled = (Boolean)$boolean;
    }

    /**
     * Sets this user to expired.
     *
     * @param Boolean $boolean
     */
    public function setExpired($boolean)
    {
        $this->expired = (Boolean)$boolean;
    }

    public function setExpiresAt(\DateTime $date)
    {
        $this->expiresAt = $date;
    }

    /**
     * Sets the hashed password.
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Sets the super admin status
     *
     * @param Boolean $boolean
     */
    public function setSuperAdmin($boolean)
    {
        if (true === $boolean) {
            $this->addRole(self::ROLE_SUPER_ADMIN);
        } else {
            $this->removeRole(self::ROLE_SUPER_ADMIN);
        }
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    /**
     * @param \DateTime $time
     */
    public function setLastLogin(\DateTime $time)
    {
        $this->lastLogin = $time;
    }

    public function setLocked($boolean)
    {
        $this->locked = $boolean;
    }

    /**
     * Sets the confirmation token
     *
     * @param string $confirmationToken
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;
    }

    /**
     * Sets the timestamp that the user requested a password reset.
     *
     * @param \DateTime $date
     */
    public function setPasswordRequestedAt(\DateTime $date = null)
    {
        $this->passwordRequestedAt = $date;
    }

    /**
     * Gets the timestamp that the user requested a password reset.
     *
     * @return \DateTime
     */
    public function getPasswordRequestedAt()
    {
        return $this->passwordRequestedAt;
    }

    /**
     * Checks whether the password reset request has expired.
     *
     * @param integer $ttl Requests older than this many seconds will be considered expired
     * @return Boolean true if the users's password request is non expired, false otherwise
     */
    public function isPasswordRequestNonExpired($ttl)
    {
        return $this->passwordRequestedAt instanceof \DateTime &&
               $this->passwordRequestedAt->getTimestamp() + $ttl > time();
    }

    /**
     * Generates the confirmation token if it is not set.
     */
    public function generateConfirmationToken()
    {
        if (null === $this->confirmationToken) {
            $this->confirmationToken = $this->generateToken();
        }
    }

    /**
     * Generates a token.
     */
    protected function generateToken()
    {
        $bytes = false;
        if (function_exists('openssl_random_pseudo_bytes') && 0 !== stripos(PHP_OS, 'win')) {
            $bytes = openssl_random_pseudo_bytes(32, $strong);

            if (true !== $strong) {
                $bytes = false;
            }
        }

        // let's just hope we got a good seed
        if (false === $bytes) {
            $bytes = hash('sha256', uniqid(mt_rand(), true), true);
        }

        return base_convert(bin2hex($bytes), 16, 36);
    }

    public function setRoles(array $roles)
    {
          $this->roles = array();

        foreach ($roles as $role) {
            $this->addRole($role);
        }
    }

    public function __toString()
    {
        return (string) $this->getUsername();
    }

    /**
     * Get the full name of the user
     * @return string
     */
    public function getFullName()
    {
        return $this->getFirstName() . " " . $this->getLastName();
    }

    /**
     * Set facebook data for user
     *
     * @param array $data
     */
    public function setFacebookData($data)
    {
        if (isset($data["id"]) && !$this->getFacebookId()) {

            $this->setFacebookId($data["id"]);
            $this->addRole("ROLE_FACEBOOK");

            $this->setLocked(false);
            $this->setExpired(false);
            $this->setCredentialsExpired(false);

            if(!isset($data["username"])) {
                $data["username"] = $data["id"];
            }
        }

        if($data['id'] === $this->getFacebookId())
        {
            if (isset($data["username"])) {
                $this->setUsername($data["username"]);
                $this->setFacebookUsername($data["username"]);
                $this->setEmail($data["username"].'@facebook.com');
            }
            if (isset($data["first_name"])) {
                $this->setFirstName($data["first_name"]);
            }
            if (isset($data["last_name"])) {
                $this->setLastName($data["last_name"]);
            }
            if (isset($data["email"])) {
                $this->setEmail($data["email"]);
            }
            if (isset($data["locale"]) && (preg_match("/[a-zA-Z]{1,8}(-[a-zA-Z0-9]{1,8})*/", $data["locale"]))) {
                $this->setLocale($data["locale"]);
            }
            if (isset($data["timezone"]) && ($timezone = timezone_name_from_abbr("", ($data["timezone"] * 3600)))) {
                $this->setTimezone($timezone);
            }
            if (isset($data["facebook_access_token"])) {
                $this->setFacebookAccessToken($data["facebook_access_token"]);
            }
        }
    }

    /**
     * Set FacebookData
     *
     * @param array $data
     */
    public function setTwitterData($data)
    {
        if (isset($data->id_str)) {
            $this->addRole("ROLE_TWITTER");
        }

        $this->setLocked(false);
        $this->setExpired(false);
        $this->setCredentialsExpired(false);
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * Get firstName
     *
     * @return string $firstName
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * Get lastName
     *
     * @return string $lastName
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set locale
     *
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Get locale
     *
     * @return string $locale
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
    }

    /**
     * Get timezone
     *
     * @return string $timezone
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set facebookId
     *
     * @param integer $facebookId
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
    }

    /**
     * Get facebookId
     *
     * @return integer $facebookId
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * Set facebookUsername
     *
     * @param string $facebookUsername
     */
    public function setFacebookUsername($facebookUsername)
    {
        $this->facebookUsername = $facebookUsername;
    }

    /**
     * Get facebookUsername
     *
     * @return string $facebookUsername
     */
    public function getFacebookUsername()
    {
        return $this->facebookUsername;
    }

    /**
     * Set facebookAccessToken
     *
     * @param string $facebookAccessToken
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebookAccessToken = $facebookAccessToken;
    }

    /**
     * Get facebookAccessToken
     *
     * @return string $facebookAccessToken
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }

    /**
     * Add twitterUsers
     *
     * @param TwitterUser $twitterUser
     */
    public function addTwitterUser(TwitterUser $twitterUser)
    {
        $this->twitterUsers[] = $twitterUser;
    }

    /**
     * Get twitterUsers
     *
     * @return Doctrine\Common\Collections\Collection $twitterUsers
     */
    public function getTwitterUsers()
    {
        return $this->twitterUsers;
    }

    /**
     * Add connections
     *
     * @param Connection $connection
     */
    public function addConnection(Connection $connection)
    {
        $this->connections[] = $connection;
    }

    /**
     * Get connections
     *
     * @return Doctrine\Common\Collections\Collection $connections
     */
    public function getConnections()
    {
        return $this->connections;
    }


    /**
     * Serializes the user.
     *
     * The serialized data have to contain the fields used by the equals method and the username.
     *
     * @return string
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->usernameCanonical,
            $this->email,
            $this->emailCanonical,
            $this->enabled,
            $this->algorithm,
            $this->salt,
            $this->password,
            $this->locked,
            $this->expired,
            $this->credentialsExpired,
            $this->firstName,
            $this->lastName,
            $this->locale,
            $this->timezone,
            $this->facebookId,
            $this->facebookUsername,
            $this->facebookAccessToken,
            $this->roles,
            $this->expiresAt,
            $this->createdAt,
            $this->updatedAt
        ));
    }

    /**
     * Unserializes the user.
     *
     * @param string $serialized
     */
    public function unserialize($serialized)
    {

        /*
         *
         */
        list(
            $this->id,
            $this->username,
            $this->usernameCanonical,
            $this->email,
            $this->emailCanonical,
            $this->enabled,
            $this->algorithm,
            $this->salt,
            $this->password,
            $this->locked,
            $this->expired,
            $this->credentialsExpired,
            $this->firstName,
            $this->lastName,
            $this->locale,
            $this->timezone,
            $this->facebookId,
            $this->facebookUsername,
            $this->facebookAccessToken,
            $this->roles,
            $this->expiresAt,
            $this->createdAt,
            $this->updatedAt
        ) = unserialize($serialized);
    }


    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Get expired
     *
     * @return boolean
     */
    public function getExpired()
    {
        return $this->expired;
    }

    /**
     * Get expiresAt
     *
     * @return datetime
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * Get credentialsExpired
     *
     * @return boolean
     */
    public function getCredentialsExpired()
    {
        return $this->credentialsExpired;
    }

    /**
     * Get credentialsExpireAt
     *
     * @return datetime
     */
    public function getCredentialsExpireAt()
    {
        return $this->credentialsExpireAt;
    }
}