<?php

namespace SnowballFactory\TweetpostBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\EntityChoiceField;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

use SnowballFactory\TweetpostBundle\Api\Facebook;
use SnowballFactory\TweetpostBundle\Api\Twitter;
use SnowballFactory\TweetpostBundle\Entity\User;

use Symfony\Component\Form\Extension\Core\ChoiceList\SimpleChoiceList;

class ConnectionType extends AbstractType
{
    private $em;
    private $user;
    private $facebook;
    private $twitter;

    public function __construct(EntityManager $em, User $user, Facebook $facebook, Twitter $twitter)
    {
        $this->em       = $em;
        $this->user     = $user;
        $this->facebook = $facebook;
        $this->twitter  = $twitter;
    }

    public function getDefaultOptions()
    {
        return array(
            'data_class' => 'SnowballFactory\TweetpostBundle\Entity\Connection',
        );
    }

    public function getName()
    {
        return 'connection';
    }

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('name', 'text', array('label' => 'Name'));
        $builder->add('active', 'hidden', array('data' => 'true'));
        $builder->add('twitter', 'entity', array('class' => 'SnowballFactory\\TweetpostBundle\\Entity\\TwitterUser', 'query_builder' => $this->em->getRepository("SnowballFactoryTweetpostBundle:TwitterUser")->createQueryBuilder('u')->where('u.user = :user')->setParameter('user', $this->user->getId())), array('label' => 'Twitter'));
        $builder->add('facebookId', 'choice', array('choices' => $this->facebook->getAccountChoices()), array('label' => 'Facebook'));
        $builder->add('postLinks', 'checkbox', array('label' => 'If your tweet contains a link, post it as a link with a preview to your wall', 'required'  => false));
        $builder->add('convertNames', 'checkbox', array('label' => "Convert Twitter's @names to real names when possible", 'required'  => false));
        $builder->add('filterHashtag', 'checkbox', array('label' => 'Only post tweets that contain #fb', 'required'  => false));
        $builder->add('actionLink', 'checkbox', array('label' => 'Add Twitter profile link to Facebook posts', 'required'  => false));
        $builder->add('disableRetweet', 'checkbox', array('label' => 'Do not post retweets', 'required'  => false));
        $builder->add('awesmTracking', 'checkbox', array('label' => 'Convert links to Awe.sm links', 'required'  => false));
        $builder->add('awesmApiKey', 'text', array('label' => 'Awe.sm API Key', 'required'  => false));
        $builder->add('blacklist', 'choice', array('choice_list' => new SimpleChoiceList($this->twitter->getBlacklistChoices(50)), 'multiple' => true, 'expanded' => true), array('label' => 'Blacklist'));
    }
}
