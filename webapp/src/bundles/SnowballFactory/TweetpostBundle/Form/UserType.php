<?php

namespace SnowballFactory\TweetpostBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

use \IntlDateFormatter;

class UserType extends AbstractType
{
    public function getDefaultOptions()
    {
        return array(
            'data_class' => 'SnowballFactory\TweetpostBundle\Entity\User',
        );
    }

    public function getName()
    {
        return 'user';
    }

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('facebookUsername', 'text', array('label' => 'Facebook', 'read_only' => true));

        $builder->add('firstName', 'text', array('label' => 'First Name'));
        $builder->add('lastName', 'text', array('label' => 'Last Name'));
        $builder->add('email');

        $builder->add('timezone', 'timezone');
        $builder->add('locale', 'locale');
    }
}
