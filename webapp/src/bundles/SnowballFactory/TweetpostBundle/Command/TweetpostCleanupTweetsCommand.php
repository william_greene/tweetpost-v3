<?php

namespace SnowballFactory\TweetpostBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use SnowballFactory\TweetpostBundle\Engine\Publisher;

use SnowballFactory\TweetpostBundle\Entity\User;
use SnowballFactory\TweetpostBundle\Entity\TwitterUser;
use SnowballFactory\TweetpostBundle\Entity\Connection;
use SnowballFactory\TweetpostBundle\Entity\Tweet;

use \DateTime;

class TweetpostCleanupTweetsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('tweetpost:cleanup:tweets')
             ->setDescription('Tweetpost engine command for truncating tweets + posts older than three months.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $facebook = $this->getContainer()->get('fos_facebook.api');
        $twitter = $this->getContainer()->get('fos_twitter.api');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        // truncate tweets + related facebook posts older than 3 months
        $tweets = $em->createQuery('SELECT t FROM SnowballFactory\TweetpostBundle\Entity\Tweet t where t.updatedAt < :date ORDER BY t.updatedAt')->setParameter('date', new DateTime('-3 months'));
        $result = $tweets->iterate();
        while (($row = $result->next()) !== false) {

            $em->remove($row[0]);
        }

        $em->flush();

    }
}
