<?php

namespace SnowballFactory\TweetpostBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use SnowballFactory\TweetpostBundle\Engine\Publisher;

use SnowballFactory\TweetpostBundle\Entity\User;
use SnowballFactory\TweetpostBundle\Entity\TwitterUser;
use SnowballFactory\TweetpostBundle\Entity\Connection;

use DateTime;

class TweetpostCleanupTwitterCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('tweetpost:cleanup:twitter')
             ->setDescription('Tweetpost engine command for cleaning up twitter users and validating tokens.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $twitter = $this->getContainer()->get('fos_twitter.api');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        // twitter users
        $twitterUsers = $em->createQuery('SELECT t FROM SnowballFactory\TweetpostBundle\Entity\TwitterUser t WHERE t.updatedAt > :date ORDER BY t.updatedAt DESC')->setParameter('date', new DateTime('-1 day'));
        $result = $twitterUsers->iterate();
        while (($row = $result->next()) !== false) {

            $twitterUser = $row[0];

            // upgrade twitter user profile
            $twitterProfile = $twitter->get('users/lookup', array('user_id' => $twitterUser->getTwitterId()));
            if(is_array($twitterProfile))
            {
                $twitterUser->setUsername($twitterProfile[0]->screen_name);
                $twitterUser->setPrivate($twitterProfile[0]->protected);
                $twitterUser->setName($twitterProfile[0]->name);
                $twitterUser->setPicture($twitterProfile[0]->profile_image_url);
            }
            else
            {
                // remove twitter users that no longer exist

                // $em->remove($twitterUser);
                // $em->flush();
                // continue;
            }

            /* Check twitter oauth tokens are valid (only migrate valid twitter accounts with posting configurations) */
            $twitter->setOAuthToken($twitterUser->getAccessToken(), $twitterUser->getAccessTokenSecret());
            $twitterProfile = $twitter->get('account/verify_credentials');
            if(is_array($twitterProfile))
            {
                $twitterUser->setUsername($twitterProfile[0]->screen_name);
                $twitterUser->setPrivate($twitterProfile[0]->protected);
                $twitterUser->setName($twitterProfile[0]->name);
                $twitterUser->setPicture($twitterProfile[0]->profile_image_url);
            }

            // check for protected accounts if access_token is null, if so polling=false
            $token = $twitterUser->getAccessToken();
            if(empty($token))
            {
                $twitterUser->setPolling(!$twitterUser->getPrivate());
            }

            $em->persist($twitterUser);
            $em->flush();

            if($input->getOption('verbose'))
            {
                $output->writeln('updated: '.$twitterUser->getUsername());
            }
        }
    }
}
