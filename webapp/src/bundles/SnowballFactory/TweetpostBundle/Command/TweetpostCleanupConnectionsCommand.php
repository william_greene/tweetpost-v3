<?php

namespace SnowballFactory\TweetpostBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use SnowballFactory\TweetpostBundle\Engine\Publisher;

use SnowballFactory\TweetpostBundle\Entity\User;
use SnowballFactory\TweetpostBundle\Entity\TwitterUser;
use SnowballFactory\TweetpostBundle\Entity\Connection;

use DateTime;

class TweetpostCleanupConnectionsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('tweetpost:cleanup:connections')
             ->setDescription('Tweetpost engine command for cleaning up facebook connections and validating tokens.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $facebook = $this->getContainer()->get('fos_facebook.api');
        $twitter = $this->getContainer()->get('fos_twitter.api');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        // check all connection profiles / tokens - upgrade if required, disable if failed
        $connections = $em->createQuery('SELECT c FROM SnowballFactory\TweetpostBundle\Entity\Connection c WHERE c.updatedAt > :date ORDER BY c.updatedAt')->setParameter('date', new DateTime('-1 day'));
        $result = $connections->iterate();
        while (($row = $result->next()) !== false) {

            $connection = $row[0];

            try {

                // set connection token to user token
                $connection->setType($connection->getType() ? $connection->getType() : 'profile');
                $connection->setAccessToken($connection->getAccessToken() ? $connection->getAccessToken() : $connection->getUser()->getFacebookAccessToken());

                // if connection is an app or page - update token
                $facebook->setAccessToken($connection->getUser()->getFacebookAccessToken());
                $tokens = $facebook->api('/me/accounts');
                if(isset($tokens['data']))
                {
                    foreach($tokens['data'] as $appOrPage)
                    {
                        if($appOrPage['id'] == $connection->getFacebookId())
                        {
                            $connection->setType(($appOrPage['category'] === 'Application' && isset($appOrPage['name'])) ? 'app' : 'page');
                            $connection->setAccessToken($appOrPage['access_token']);
                        }
                    }
                }

                $em->persist($connection);
                $em->flush();
            }
            catch (\Exception $e)
            {
                // $connection->setActive(false);
                // $connection->setAccessToken(null);
            }

            if($input->getOption('verbose'))
            {
                $output->writeln('updated: '.$this->getContainer()->get('serializer')->serialize($connection, 'json'));
            }
        }

    }
}
