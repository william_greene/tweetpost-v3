<?php

namespace SnowballFactory\TweetpostBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use SnowballFactory\TweetpostBundle\Engine\Consumer;

class TweetpostConsumerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('tweetpost:consumer')->setDescription('Tweetpost engine command for managing consumption of Twitter APIs');
    }

    protected function execute(InputInterface $input,  OutputInterface $output)
    {
        $this->getContainer()->get('snowball_factory_tweetpost.engine.consumer')->consume();
    }
}
