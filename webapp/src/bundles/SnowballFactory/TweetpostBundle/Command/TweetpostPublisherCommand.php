<?php

namespace SnowballFactory\TweetpostBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use \Facebook;
use \Embedly\Embedly;
use \SnowballFactory\TweetpostBundle\Awesm\Awesm;

use \Resque;
use \Resque_Worker;

use SnowballFactory\TweetpostBundle\Engine\Processor;

class TweetpostPublisherCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('tweetpost:publisher')
             ->setDescription('Tweetpost engine command for managing post publishing.')
             ->addOption('queues', 'qu', InputOption::VALUE_OPTIONAL, 'Worker Queues', 'tweetpost_tweets')
             ->addOption('count', 'c', InputOption::VALUE_OPTIONAL, 'Worker count', 1)
             ->addOption('interval', 'i', InputOption::VALUE_OPTIONAL, 'Worker interval', 3);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
      	Resque::setBackend($this->getContainer()->getParameter('cache.host').':'.$this->getContainer()->getParameter('cache.port'));

        $queues = $input->getOption('queues');
        $count = $input->getOption('count');
        $interval = $input->getOption('interval');

        $logging = $input->getOption('verbose') ? Resque_Worker::LOG_VERBOSE : Resque_Worker::LOG_NORMAL;

        if($count > 1) {
        	for($i = 0; $i < $count; ++$i) {
        		$pid = pcntl_fork();
        		if($pid == -1) {
        			throw new \Exception("Could not fork worker ".$i);
        		}
        		else if(!$pid) {
        			$worker = new Resque_Worker($queues);
        			$worker->logLevel = $logging;
        			$worker->work($interval);

        			break;
        		}
        	}
        }
        else {
        	$worker = new Resque_Worker($queues);
        	$worker->logLevel = $logging;
        	$worker->work($interval);
        }
    }
}
