<?php

namespace SnowballFactory\TweetpostBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use SnowballFactory\TweetpostBundle\Engine\Publisher;

use SnowballFactory\TweetpostBundle\Entity\User;
use SnowballFactory\TweetpostBundle\Entity\TwitterUser;
use SnowballFactory\TweetpostBundle\Entity\Connection;

use PDO;

class TweetpostMigrateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('tweetpost:migrate')
             ->setDescription('Tweetpost engine command for migrating from v2 to v3 while cleaning up users and tokens.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $facebook = $this->getContainer()->get('fos_facebook.api');
        $twitter = $this->getContainer()->get('fos_twitter.api');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $db = new PDO('mysql:host=localhost;dbname=tweetface', 'tweetpost', 'tw33tFence');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $migratedFacebookIds = $em->createQuery('SELECT DISTINCT u.facebookId FROM SnowballFactory\TweetpostBundle\Entity\User u')->getResult();

        $ids = array();
        foreach($migratedFacebookIds as $migratedFacebookId)
        {
          $ids[] = $migratedFacebookId['facebookId'];
        }

        /* Find users */
        $users = $db->prepare('SELECT * FROM users WHERE fb_user_id IS NOT NULL and fb_session_key IS NOT NULL AND fb_user_id IN (SELECT DISTINCT fb_user_id from users WHERE fb_user_id NOT IN ('.implode(',', $ids).')) ORDER BY users.created_at ASC');
        $users->execute();
        while ($originalUser = $users->fetch(PDO::FETCH_ASSOC)) {

            $user = $em->createQuery('select u.id from SnowballFactory\TweetpostBundle\Entity\User u where u.facebookId = :facebookId')->setParameter('facebookId', $originalUser['fb_user_id'])->getResult();
            if(empty($user))
            {
                if($input->getOption('verbose'))
                {
                    $output->writeln('importing: '.$originalUser['fb_user_id']."\n");
                }

                // public facebook profile (id, first + last name, username, fb email, locale)
                $facebookProfile = $facebook->api('/'.$originalUser['fb_user_id']);
                if(!empty($facebookProfile))
                {
                    $username = empty($facebookProfile['username']) ? $originalUser['fb_user_id'] : $facebookProfile['username'];

                    $user = $this->getContainer()->get('fos_user.user_manager')->createUser();
                    $user->setEnabled(true);
                    $user->setLocked(false);
                    $user->setExpired(false);
                    $user->setCredentialsExpired(false);
                    $user->setPassword($this->getContainer()->get('security.encoder_factory')->getEncoder($user)->encodePassword('tweetpost', $user->getSalt()));
                    $user->setUsername($username);
                    $user->setEmail($user->getUsername().'@'.'facebook.com');
                    $user->setFirstName($facebookProfile['first_name']);
                    $user->setLastName($facebookProfile['last_name']);
                    $user->setLocale($facebookProfile['locale']);
                    $user->setTimezone('America/Los_Angeles');
                    $user->setFacebookAccessToken($originalUser['fb_session_key']);
                    $user->setFacebookId($originalUser['fb_user_id']);
                    $user->setFacebookUsername($username);
                    $user->addRole('ROLE_USER');
                    $user->addRole('ROLE_FACEBOOK');

                    /* save user */
                    $this->getContainer()->get('fos_user.user_manager')->updateUser($user);


                    /* Find twitter accounts associated with users */
                    $twitterAccounts = $db->prepare('SELECT * FROM twitter_accounts WHERE user_id = '.$originalUser['id']);
                    $twitterAccounts->execute();
                    while ($originalTwitterUser = $twitterAccounts->fetch(PDO::FETCH_ASSOC)) {

                        $twitterUser = $em->createQuery('select c.id from SnowballFactory\TweetpostBundle\Entity\TwitterUser c where c.user = :user and c.twitterId = :twitterId')->setParameter('user', $user->getId())->setParameter('twitterId', $originalTwitterUser['twitter_user_id'])->getResult();
                        if(empty($twitterUser))
                        {
                            $twitterUser = new TwitterUser();
                            $twitterUser->setUser($user);
                            $twitterUser->setTwitterId($originalTwitterUser['twitter_user_id']);
                            $twitterUser->setPolling($originalTwitterUser['polling']);
                            $twitterUser->setUsername($originalTwitterUser['twitter_username']);
                            $twitterUser->setPrivate($originalTwitterUser['twitter_private']);
                            $twitterUser->setName($originalTwitterUser['twitter_real_name']);
                            $twitterUser->setPicture($originalTwitterUser['twitter_picture']);
                            $twitterUser->setAccessToken($originalTwitterUser['oauth_token']);
                            $twitterUser->setAccessTokenSecret($originalTwitterUser['oauth_verifier']);

                            $user->getTwitterUsers()->add($twitterUser);
                            $em->persist($twitterUser);
                            $em->flush();

                            /* Find posting configurations from valid twitter credentials associated with users */
                            $configurations = $db->prepare('SELECT * FROM posting_configurations LEFT JOIN authorized_facebook_spots on posting_configurations.authorized_facebook_spot_id = authorized_facebook_spots.id LEFT JOIN users on posting_configurations.user_id = users.id WHERE posting_configurations.twitter_account_id = '.$originalTwitterUser['id'].' AND posting_configurations.user_id = '.$originalUser['id']);
                            $configurations->execute();
                            while ($originalConfiguration = $configurations->fetch(PDO::FETCH_ASSOC)) {

                              $connection = $em->createQuery('select c.id from SnowballFactory\TweetpostBundle\Entity\Connection c where c.user = :user and c.twitter = :twitter and c.facebookId = :facebookId and c.type = :type')->setParameter('user', $user->getId())->setParameter('twitter', $twitterUser->getId())->setParameter('facebookId', $originalConfiguration['fb_id'])->setParameter('type', $originalConfiguration['page_type'])->getResult();
                              if(empty($connection)) {

                                  $connection = new Connection();
                                  $connection->setUser($user);
                                  $connection->setTwitter($twitterUser);
                                  $connection->setActive($originalTwitterUser['polling']);
                                  $connection->setFacebookId($originalConfiguration['fb_id']);
                                  $connection->setType($originalConfiguration['page_type']);
                                  $connection->setName($originalConfiguration['title']);
                                  $connection->setConvertNames(isset($originalConfiguration['use_real_names']) ? true : false);
                                  $connection->setPostLinks(isset($originalConfiguration['post_as_notes']) ? true : false);
                                  $connection->setFilterHashtag(isset($originalConfiguration['explicitly_tagged']) ? true : false);
                                  $connection->setActionLink(isset($originalConfiguration['action_link']) ? true : false);
                                  $connection->setAwesmTracking(isset($originalConfiguration['awesm_powered']) ? true : false);
                                  $connection->setAwesmApiKey($originalConfiguration['awesm_api_key']);
                                  $connection->setDisableRetweet(false);
                                  $connection->setBlacklist(array());
                                  $connection->setAccessToken($originalUser['fb_session_key']);

                                  $user->getConnections()->add($connection);
                                  $em->persist($connection);
                                  $em->flush();

                              }

                            }
                        }
                    }

                    if($input->getOption('verbose'))
                    {
                        $output->writeln('importing: '.$user->getUsername()."\n");
                    }
                }
            }
        }

        $output->writeln('Done importing and migrating legacy users!');
    }
}
