<?php

namespace SnowballFactory\TweetpostBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use SnowballFactory\TweetpostBundle\Engine\Publisher;

use SnowballFactory\TweetpostBundle\Entity\User;
use SnowballFactory\TweetpostBundle\Entity\TwitterUser;
use SnowballFactory\TweetpostBundle\Entity\Connection;

use PDO;
use DateTime;

class TweetpostCleanupUsersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('tweetpost:cleanup:users')
             ->setDescription('Tweetpost engine command for cleaning up users and tokens.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $facebook = $this->getContainer()->get('fos_facebook.api');
        $twitter = $this->getContainer()->get('fos_twitter.api');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        // check all user facebook profile / tokens - upgrade if required
        $users = $em->createQuery('SELECT u FROM SnowballFactory\TweetpostBundle\Entity\User u WHERE u.updatedAt > :date AND u.facebookAccessToken IS NOT NULL AND u.facebookAccessToken NOT LIKE :like and u.facebookAccessToken LIKE :like2 ORDER BY u.createdAt ASC')->setParameter('date', new DateTime('-1 day'))->setParameter('like', "%.3600.%")->setParameter('like2', "%-%");
        $result = $users->iterate();
        while (($row = $result->next()) !== false) {

            $user = $row[0];

            /* Check facebook user oauth tokens and upgrade to 2.0 sessions */
            try {
                $facebookUserProfile = $facebook->api('/'.$user->getFacebookId());
                if(empty($facebookUserProfile))
                {
                    // remove facebook users that no longer exist

                    // $em->remove($user);
                    // $em->flush();
                    // continue;
                }

                $user->setFacebookData($facebookUserProfile);
                $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

                if(false !== strpos($user->getFacebookAccessToken(), '-'))
                {
                    $session = array('client_id' => $facebook->getAppId(), 'client_secret' => $facebook->getAppSecret(), 'sessions' => $user->getFacebookAccessToken());
                    $token = $facebook->api('/oauth/exchange_sessions', 'POST', $session);

                    $facebook->setAccessToken($token[0]['access_token']);
                    $facebookUserProfile = $facebook->api('/me');

                    $user->setFacebookAccessToken($token[0]['access_token']);
                    $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

                    if($input->getOption('verbose'))
                    {
                        $output->writeln('token updated: ...'."\n");
                    }
                }

                try {
                    $facebook->setAccessToken($user->getFacebookAccessToken());
                    $facebookUserProfile = $facebook->api('/me');

                    $user->setFacebookData($facebookUserProfile);
                    $this->getContainer()->get('fos_user.user_manager')->updateUser($user);

                    if($input->getOption('verbose'))
                    {
                        $output->writeln('updated profile from live token: '."\n");
                    }
                }
                catch(\Exception $e)
                {
                    // token is invalid or can not be upgraded so access token set null
                    // $user->setFacebookAccessToken(null);
                    if($input->getOption('verbose'))
                    {
                        $output->writeln('token invalid: ...'."\n");
                    }
                }

                if($input->getOption('verbose'))
                {
                    $output->writeln('updated profile from live token: '.$this->getContainer()->get('serializer')->serialize($user, 'json')."\n");
                }
            }
            catch (\Exception $e)
            {
                if($input->getOption('verbose'))
                {
                    $output->writeln('failed: '.$this->getContainer()->get('serializer')->serialize($user, 'json')."\n");
                }
            }

            $this->getContainer()->get('fos_user.user_manager')->updateUser($user);
        }

    }
}
