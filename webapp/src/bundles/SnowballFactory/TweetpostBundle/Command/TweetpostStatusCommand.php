<?php

namespace SnowballFactory\TweetpostBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use SnowballFactory\TweetpostBundle\Engine\Publisher;

use Resque;
use Resque_Stat;
use Resque_Job_Status;

class TweetpostStatusCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('tweetpost:status')
             ->setDescription('Tweetpost engine command for managing engine status.')
             ->addOption('queue', 'qu', InputOption::VALUE_OPTIONAL, 'Worker Queue', 'tweetpost_tweets')
             ->addOption('reset', 'r', InputOption::VALUE_OPTIONAL, 'Reset stats', false)
             ->addArgument('id', InputArgument::OPTIONAL, 'Job ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
      	Resque::setBackend($this->getContainer()->getParameter('cache.host').':'.$this->getContainer()->getParameter('cache.port'));

        $queue = $input->getOption('queue');
        $id = $input->getArgument('id');

        if(empty($id)) {
          $output->writeln("Jobs in queue {$queue}: ".Resque::size($queue));

          $output->writeln("Jobs failed: ".Resque_Stat::get('failed'));
          $output->writeln("Jobs processed: ".Resque_Stat::get('processed'));

          $reset = $input->getOption('reset');
          if($reset)
          {
            Resque_Stat::clear('failed');
            Resque_Stat::clear('processed');

            $output->writeln("Reset stats!");
          }
        }
        else {

          $status = new Resque_Job_Status($id);
          if(!$status->isTracking()) {
          	throw new \Exception("Resque is not tracking the status of this job.\n");
          }

          while(true) {
            $output->writeln("Status of TweetPo.st job ".$id." is: ".$status->get());
          	sleep(1);
          }

        }

    }
}
