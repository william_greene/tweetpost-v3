<?php

namespace SnowballFactory\TweetpostBundle\Repository;

use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

use Doctrine\ORM\EntityRepository;


class UserRepository extends EntityRepository implements UserProviderInterface
{
    public function findAllOrderedByName()
    {
        return $this->getEntityManager()->createQuery('SELECT u FROM SnowballFactory\TweetpostBundle\Entity\User u ORDER BY u.username ASC')->getResult();
    }

    public function findRecentlyLoggedIn($limit = 10, $offset = 0)
    {
        $query = $this->getEntityManager()->createQuery('SELECT u FROM SnowballFactory\TweetpostBundle\Entity\User u WHERE u.lastLogin IS NOT NULL ORDER BY u.lastLogin DESC');

        $query->setMaxResults($limit);
        $query->setFirstResult($offset);

        return $query->getResult();
    }

    public function supportsClass($class)
    {
        return $class === $this->getClass();
    }

    /**
     * Loads a user by user
     *
     * It is strongly discouraged to use this method manually as it bypasses
     * all ACL checks.
     *
     * @param SecurityUserInterface $user
     * @return User
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException('User is not supported.');
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * Loads a user by username
     *
     * It is strongly discouraged to call this method manually as it bypasses
     * all ACL checks.
     *
     * @extra:RunAs(roles="ROLE_SUPERADMIN")
     * @param string $username
     * @return SecurityUserInterface
     */
    public function loadUserByUsername($username)
    {
        $user = $this->findUserByUsername($username);

        if (!$user) {
            throw new UsernameNotFoundException(sprintf('No user with name "%s" was found.', $username));
        }

        return $user;
    }
}
