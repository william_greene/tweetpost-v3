<?php

namespace SnowballFactory\TweetpostBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ContentController extends Controller
{
    /**
     * @Route("/", name="tweetpost_homepage")
     * @Template()
     */
    public function homepageAction()
    {
        return array();
    }
    
    /**
     * @Route("/privacy-policy", name="tweetpost_privacy_policy")
     * @Template()
     */
    public function privacyAction()
    {
        return array();
    }
    
    /**
     * @Route("/terms-of-service", name="tweetpost_terms_of_use")
     * @Template()
     */
    public function tosAction()
    {
        return array();
    }
}
