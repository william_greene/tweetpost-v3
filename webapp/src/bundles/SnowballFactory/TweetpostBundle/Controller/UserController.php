<?php

namespace SnowballFactory\TweetpostBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use SnowballFactory\TweetpostBundle\Form\UserType;

use \Exception;


/**
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/login", name="tweetpost_user_login")
     * @Template()
     */
    public function loginAction()
    {
        if ($this->get("request")->attributes->has(SecurityContext::AUTHENTICATION_ERROR))
        {
            $error = $this->get("request")->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        }
        else
        {
            $error = $this->get("request")->getSession()->get(SecurityContext::AUTHENTICATION_ERROR);
        }

        if($error)
        {
            $this->get("session")->setFlash("notification", $error->getMessage());
            return new RedirectResponse($this->generateUrl("tweetpost_homepage"));
        }

        return array(
            "last_username" => $this->get("request")->getSession()->get(SecurityContext::LAST_USERNAME)
        );
    }

    /**
     * @Route("/authorize", name="tweetpost_user_authorize")
     */
    public function authorizeAction()
    {
        // The security layer will intercept this request
        return new RedirectResponse($this->generateUrl("tweetpost_connection_dashboard"));
    }

    /**
     * @Route("/unauthorized", name="tweetpost_user_unauthorized")
     * @Template()
     */
    public function unauthorizedAction()
    {
        return array("requiredRoles" => array());
    }

    /**
     * @Route("/logout", name="tweetpost_user_logout")
     */
    public function logoutAction()
    {
        // The security layer will intercept this request
        return new RedirectResponse($this->generateUrl("tweetpost_homepage"));
    }

    /**
     * @Route("/facebook/login", name="tweetpost_user_facebook_login")
     * @Template()
     */
    public function facebookLoginAction()
    {
        return $this->get("fos_facebook.security.authentication.entry_point.main")->start($this->get("request"));
    }

    /**
     * @Route("/facebook/authorize", name="tweetpost_user_facebook_authorize")
     */
    public function facebookAuthorizeAction()
    {
        // The security layer will intercept this request
        return new RedirectResponse($this->generateUrl("tweetpost_connection_dashboard"));
    }

    /**
     * @Route("/twitter/login", name="tweetpost_user_twitter_login")
     * @Template()
     */
    public function twitterLoginAction()
    {
        $request = $this->get("request");
        $twitter = $this->get("fos_twitter.api");

        $requestToken = $twitter->getRequestToken($request->getUriForPath("/user/twitter/authorize"));

        $session = $request->getSession();
        $session->set("oauth_token", $requestToken["oauth_token"]);
        $session->set("oauth_token_secret", $requestToken["oauth_token_secret"]);

        return new RedirectResponse($twitter->getAuthorizeURL($requestToken));
    }

    /**
     * @Route("/twitter/authorize", name="tweetpost_user_twitter_authorize")
     */
    public function twitterAuthorizeAction()
    {
        // The security layer will intercept this request
        return new RedirectResponse($this->generateUrl("tweetpost_connection_dashboard"));
    }

    /**
     * @Route("/profile/{username}", name="tweetpost_user_profile")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function profileAction($username)
    {
        $entityManager = $this->get("doctrine")->getEntityManager();
        $request = $this->get("request");
        $user = $this->get("security.context")->getToken()->getUser();

        /* only users can view/edit their own profiles */
        if(($username !== $user->getUsername()))
        {
            throw new AccessDeniedException();
        }

        $form = $this->get("form.factory")->create(new UserType(), $user);
        if ($request->getMethod() == "POST")
        {
            $form->bindRequest($request);
            if ($form->isValid())
            {
                $entityManager->persist($user);
                $entityManager->flush();

                $this->get("session")->setFlash("notification", "Your profile details have been updated.");

                return $this->redirect($this->generateUrl("tweetpost_user_profile", array("username" => $user->getUsername())));
            }
        }

        return array("user" => $user, "twitterUsers" => $user->getTwitterUsers()->toArray(), "connections" => $user->getConnections()->toArray(), "form" => $form->createView());
    }

    /**
     * @Route("/delete", name="tweetpost_user_delete")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function deleteAction()
    {
        $entityManager = $this->get("doctrine")->getEntityManager();
        $user = $this->get("security.context")->getToken()->getUser();

        $this->get("session")->setFlash("notification", "Your user has been deleted.");

        $entityManager->remove($user);
        $entityManager->flush();

        return new RedirectResponse($this->generateUrl("tweetpost_user_logout"));
    }
}
