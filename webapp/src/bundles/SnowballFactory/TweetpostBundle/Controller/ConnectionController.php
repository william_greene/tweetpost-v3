<?php

namespace SnowballFactory\TweetpostBundle\Controller;

use SnowballFactory\TweetpostBundle\Api\Twitter;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

use FOS\UserBundle\Model\UserManager;

use SnowballFactory\TweetpostBundle\Entity\User;
use SnowballFactory\TweetpostBundle\Entity\TwitterUser;
use SnowballFactory\TweetpostBundle\Entity\Connection;

use SnowballFactory\TweetpostBundle\Form\UserType;
use SnowballFactory\TweetpostBundle\Form\ConnectionType;

use \Embedly_API;
use SnowballFactory\TweetpostBundle\Engine\Processor;

use \Exception;
use \FacebookApiException;


/**
 * @Route("/connection")
 */
class ConnectionController extends Controller
{
    /**
     * @Route("/dashboard", name="tweetpost_connection_dashboard")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function dashboardAction()
    {
        $connections = $this->get("security.context")->getToken()->getUser()->getConnections()->toArray();

        return array('connections' => $connections, 'connectionsCount' => count($connections));
    }

    /**
     * @Route("/create", name="tweetpost_connection_create")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function createAction()
    {
        $request = $this->get("request");
        $user = $this->get("security.context")->getToken()->getUser();
        $entityManager = $this->get("doctrine")->getEntityManager();

        $facebook = $this->get('fos_facebook.api');
        $twitter = $this->get('fos_twitter.api');

        $twitterUsers = $user->getTwitterUsers()->toArray();
        $selected = count($twitterUsers) - 1;

        if(!isset($twitterUsers[0])) {
            return new RedirectResponse($this->generateUrl('tweetpost_connection_twitter_connect'));
        }

        $twitter->setOAuthToken($twitterUsers[$selected]->getAccessToken(), $twitterUsers[$selected]->getAccessTokenSecret());
        $facebook->setAccessToken($user->getFacebookAccessToken());

        /* default state for connections */
        $connection = new Connection();
        $connection->setActive(true);
        $connection->setUser($user);
        $connection->setTwitter($twitterUsers[$selected]);
        $connection->setName('@'.$twitterUsers[$selected]->getUsername().' to '.$user->getFirstName().' '.$user->getLastName());
        $connection->setConvertNames(true);
        $connection->setPostLinks(true);
        $connection->setActionLink(true);
        $connection->setDisableRetweet(false);
        $connection->setFilterHashtag(false);
        $connection->setAwesmTracking(false);

        $form = $this->get("form.factory")->create(new ConnectionType($entityManager, $user, $facebook, $twitter), $connection);
        if ($request->getMethod() == "POST")
        {
            $form->bind($this->get('request')->request->get('connection'));
            if ($form->isValid())
            {
                $tokens = $facebook->api('/'.$user->getFacebookId().'/accounts');
                if(isset($tokens['data']))
                {
                    foreach($tokens['data'] as $appOrPage)
                    {
                        if($appOrPage['id'] == $connection->getFacebookId())
                        {
                            $connection->setFacebookId($appOrPage['id']);
                            $connection->setType(($appOrPage['category'] === 'Application' && isset($appOrPage['name'])) ? 'app' : 'page');
                            $connection->setAccessToken($appOrPage['access_token']);
                        }
                    }
                }

                $type = $connection->getType();
                if(empty($type))
                {
                    $connection->setType('profile');
                    $connection->setFacebookId($user->getFacebookId());
                    $connection->setAccessToken($user->getFacebookAccessToken());
                }

                $user->getConnections()->add($connection);

                $entityManager->persist($connection);
                $entityManager->flush();

                $this->get("session")->setFlash("notification", sprintf("The tweetpost connection %s has been created.", $connection->getName()));

                return $this->redirect($this->generateUrl("tweetpost_connection_dashboard"));
            }
        }

        return array("form" => $form->createView());
    }

    /**
     * @Route("/edit/{id}", name="tweetpost_connection_edit")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function editAction($id)
    {
        $request = $this->get("request");
        $user = $this->get("security.context")->getToken()->getUser();
        $entityManager = $this->get("doctrine")->getEntityManager();

        $facebook = $this->get('fos_facebook.api');
        $twitter = $this->get('fos_twitter.api');

        $connection = $entityManager->getRepository("SnowballFactoryTweetpostBundle:Connection")->findOneBy(array("user" => $user->getId(), "id" => $id));
        if(empty($connection))
        {
            throw new NotFoundHttpException("The connection does not exist.");
        }

        $twitter->setOAuthToken($connection->getTwitter()->getAccessToken(), $connection->getTwitter()->getAccessTokenSecret());
        $facebook->setAccessToken($user->getFacebookAccessToken());

        $form = $this->get("form.factory")->create(new ConnectionType($entityManager, $user, $facebook, $twitter), $connection);
        if ($request->getMethod() == "POST")
        {
            $form->bind($this->get('request')->request->get('connection'));
            if ($form->isValid())
            {
                $tokens = $facebook->api('/'.$user->getFacebookId().'/accounts');
                if(isset($tokens['data']))
                {
                    foreach($tokens['data'] as $appOrPage)
                    {
                        if($appOrPage['id'] == $connection->getFacebookId())
                        {
                            $connection->setFacebookId($appOrPage['id']);
                            $connection->setType(($appOrPage['category'] === 'Application' && isset($appOrPage['name'])) ? 'app' : 'page');
                            $connection->setAccessToken($appOrPage['access_token']);
                        }
                    }
                }

                $entityManager->persist($connection);
                $entityManager->flush();

                $this->get("session")->setFlash("notification", sprintf("The tweetpost connection %s has been updated.", $connection->getName()));

                return $this->redirect($this->generateUrl("tweetpost_connection_dashboard"));
            }
        }

        return array("form" => $form->createView(), "connection" => $connection);
    }

    /**
     * @Route("/delete/{id}", name="tweetpost_connection_delete")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function deleteAction($id)
    {
        $request = $this->get("request");
        $user = $this->get("security.context")->getToken()->getUser();
        $entityManager = $this->get("doctrine")->getEntityManager();

        $connection = $entityManager->getRepository("SnowballFactoryTweetpostBundle:Connection")->findOneBy(array("user" => $user->getId(), "id" => $id));
        if(empty($connection))
        {
            throw new NotFoundHttpException("The tweetpost connection does not exist.");
        }

        $this->get("session")->setFlash("notification", sprintf("The tweetpost connection %s has been deleted.", $connection->getName()));

        $entityManager->remove($connection);
        $entityManager->flush();

        return $this->redirect($this->generateUrl("tweetpost_connection_dashboard"));
    }

    /**
     * @Route("/toggle/{id}", name="tweetpost_connection_toggle")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function toggleAction($id)
    {
        $request = $this->get("request");
        $user = $this->get("security.context")->getToken()->getUser();
        $entityManager = $this->get("doctrine")->getEntityManager();

        $connection = $entityManager->getRepository("SnowballFactoryTweetpostBundle:Connection")->findOneBy(array("user" => $user->getId(), "id" => $id));
        if(empty($connection))
        {
            throw new NotFoundHttpException("The connection does not exist.");
        }
        $connection->setActive(!($connection->getActive() === true));

        $entityManager->persist($connection);
        $entityManager->flush();

        $this->get("session")->setFlash("notification", sprintf("The tweetpost connection %s has been %s.", $connection->getName(), $connection->getActive() ? 'enabled' : 'disabled'));

        return $this->redirect($this->generateUrl("tweetpost_connection_dashboard"));
    }


    /**
     * @Route("/facebook/connect", name="tweetpost_connection_facebook_connect")
     * @Secure(roles="ROLE_USER")
     */
    public function facebookConnectAction()
    {
        $request  = $this->get("request");
        $user     = $this->get("security.context")->getToken()->getUser();
        $facebook = $this->get("fos_facebook.api");

        $response = new RedirectResponse($facebook->getLoginUrl(
           array(
                "cancel_url" => $request->getUriForPath("/"),
                "canvas" => "false",
                "display" => "page",
                "fbconnect" => "true",
                "req_perms" => implode(",", $this->getParameter("fos_facebook.permissions")),
                "next" => $request->getUriForPath("/connection/facebook/authenticate"),
            ))
        );

        return $response;
    }


    /**
     * @Route("/facebook/authenticate", name="tweetpost_connection_facebook_authenticate")
     * @Secure(roles="ROLE_USER")
     */
    public function facebookAuthenticateAction()
    {
        $request  = $this->get("request");
        $user     = $this->get("security.context")->getToken()->getUser();
        $facebook = $this->get("fos_facebook.api");

        $facebook->setAccessToken($user->getFacebookAccessToken());

        $session = $facebook->getSession();

        if(empty($session) || !isset($session["access_token"]))
        {
            $this->get("session")->setFlash("notification", "Facebook failed to return an access token for user.");

            return new RedirectResponse($this->generateUrl("tweetpost_homepage"));
        }

        $data = $facebook->api("/me");
        if (!empty($data)) {
            $data["facebook_access_token"] = $facebook->getAccessToken();
            $user->setFacebookData($data);

            if (count($this->get("validator")->validate($user, "Facebook"))) {
                throw new UsernameNotFoundException("The facebook user could not be stored.");
            }
            $this->get("fos_user.user_manager")->updateUser($user);
        }
        else {
            $this->get("session")->setFlash("notification", "Facebook failed to return profile for user.");

            return new RedirectResponse($this->generateUrl("tweetpost_homepage"));
        }

        $this->get("session")->setFlash("notification", sprintf("The facebook user %s has been connected.", $user->getFacebookUsername()));

        return new RedirectResponse($this->generateUrl("tweetpost_connection_dashboard"));
    }

    /**
     * @Route("/facebook/disconnect/{id}", name="tweetpost_connection_facebook_disconnect")
     * @Secure(roles="ROLE_USER")
     */
    public function facebookDisconnectAction($id)
    {
        return new RedirectResponse($this->generateUrl("tweetpost_connection_dashboard"));
    }


    /**
     * @Route("/twitter/connect", name="tweetpost_connection_twitter_connect")
     * @Secure(roles="ROLE_USER")
     */
    public function twitterConnectAction()
    {
        $request = $this->get("request");
        $twitter = $this->get("fos_twitter.api");

        /* Get temporary credentials. */
        $requestToken = $twitter->getRequestToken($request->getUriForPath("/connection/twitter/authenticate"));
        if(empty($requestToken) || ($twitter->http_code !== 200))
        {
            $this->get("session")->setFlash("notification", sprintf("Twitter failed to connect your user. (%s)", $twitter->http_code));

            return new RedirectResponse($this->generateUrl("tweetpost_connection_dashboard"));
        }

        /* Save temporary credentials (request token) to session and redirect user to authorize */
        $session = $request->getSession();
        $session->set("twitter_oauth_token", $requestToken["oauth_token"]);
        $session->set("twitter_oauth_token_secret", $requestToken["oauth_token_secret"]);

        return new RedirectResponse($twitter->getAuthorizeURL($requestToken, false));
    }

    /**
     * @Route("/twitter/authenticate", name="tweetpost_connection_twitter_authenticate")
     * @Secure(roles="ROLE_USER")
     */
    public function twitterAuthenticateAction()
    {
        $request = $this->get("request");
        $session = $request->getSession();

        $user    = $this->get("security.context")->getToken()->getUser();
        $twitter = $this->get("fos_twitter.api");

        $entityManager = $this->get('doctrine')->getEntityManager();

        /* Set OAuth request token in the API */
        $twitter->setOAuthToken($request->get("oauth_token"), $session->get("twitter_oauth_token_secret"));

        /* Remove no longer needed request tokens */
        !$session->has("twitter_oauth_token") ?: $session->remove("oauth_token");
        !$session->has("twitter_oauth_token_secret") ?: $session->remove("oauth_token_secret");

        /* Swap temporary credentials for authorized credentials. */
        $accessToken = $twitter->getAccessToken($request->get("oauth_verifier"));
        if(empty($accessToken) || ($twitter->http_code !== 200))
        {
            $this->get("session")->setFlash("notification", sprintf("Twitter failed to connect your user. (%s)", $twitter->http_code));

            return new RedirectResponse($this->generateUrl("tweetpost_connection_dashboard"));
        }

        /* Save the access tokens if they can be verified */
        $session->set("twitter_oauth_token", $accessToken["oauth_token"]);
        $session->set("twitter_oauth_token_secret", $accessToken["oauth_token_secret"]);

        /* Set OAuth access token in the API */
        $twitter->setOAuthToken($accessToken["oauth_token"], $accessToken["oauth_token_secret"]);

        $data =  $twitter->get("account/verify_credentials");
        if (!empty($data)) {

            $data->twitter_access_token = $accessToken["oauth_token"];
            $data->twitter_access_token_secret = $accessToken["oauth_token_secret"];

            /* Update user token */
            if(!empty($data->twitter_access_token))
            {
                $user->setTwitterData($data);

                if (count($this->get("validator")->validate($user, "Twitter"))) {
                    throw new UsernameNotFoundException("The twitter user could not be stored.");
                }
                $this->get("fos_user.user_manager")->updateUser($user);
            }

            $twitterUser = $entityManager->getRepository("SnowballFactoryTweetpostBundle:TwitterUser")->findOneBy(array("user" => $user->getId(), "twitterId" => $data->id_str));
            if(!$twitterUser)
            {
                /* Add new twitter user */
                $twitterUser = new TwitterUser();
                $twitterUser->setUsername($data->screen_name);
                $twitterUser->setPolling(true);
                $twitterUser->setTwitterId($data->id_str);
            }
            $twitterUser->setPrivate($data->protected);
            $twitterUser->setName($data->name);
            $twitterUser->setPicture($data->profile_image_url);
            $twitterUser->setAccessToken($data->twitter_access_token);
            $twitterUser->setAccessTokenSecret($data->twitter_access_token_secret);

            $user->getTwitterUsers()->add($twitterUser);
            $twitterUser->setUser($user);

            $entityManager->persist($twitterUser);
            $entityManager->flush();
        }
        else {
            $this->get("session")->setFlash("notification", "Twitter failed to return a profile for your user.");

            return new RedirectResponse($this->generateUrl("tweetpost_connection_dashboard"));
        }

        $this->get("session")->setFlash("notification", sprintf("The twitter user @%s has been connected.", $twitterUser->getUsername()));

        return new RedirectResponse($this->generateUrl("tweetpost_connection_create"));
    }

    /**
     * @Route("/twitter/disconnect/{id}", name="tweetpost_connection_twitter_disconnect")
     * @Secure(roles="ROLE_USER")
     */
    public function twitterDisconnectAction($id)
    {
        $request = $this->get("request");
        $user = $this->get("security.context")->getToken()->getUser();
        $entityManager = $this->get("doctrine")->getEntityManager();

        $twitterUser = $entityManager->getRepository("SnowballFactoryTweetpostBundle:TwitterUser")->findOneBy(array("user" => $user->getId(), "id" => $id));
        $connection = $entityManager->getRepository("SnowballFactoryTweetpostBundle:Connection")->findOneBy(array("user" => $user->getId(), "twitter" => $twitterUser->getId()));
        if(empty($twitterUser))
        {
            throw new NotFoundHttpException("The twitter user does not exist.");
        }

        if(!empty($connection))
        {
            $entityManager->remove($connection);
        }

        $this->get("session")->setFlash("notification", sprintf("The twitter user @%s has been deleted.", $twitterUser->getUsername()));

        $entityManager->remove($twitterUser);
        $entityManager->flush();

        return $this->redirect($this->generateUrl("tweetpost_user_profile", array('username' => $user->getUsername())));
    }

}
