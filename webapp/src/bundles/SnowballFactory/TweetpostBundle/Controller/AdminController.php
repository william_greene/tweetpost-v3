<?php

namespace SnowballFactory\TweetpostBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

use \Resque;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/dashboard", name="tweetpost_admin_dashboard")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function dashboardAction()
    {
        $entityManager = $this->get("doctrine")->getEntityManager();

        $jobsCount = Resque::size('tweetpost_tweets');

        $usersCount = $entityManager->createQuery('SELECT COUNT(u.id) FROM SnowballFactory\TweetpostBundle\Entity\User u')->getSingleScalarResult();
        $twitterUsersCount = $entityManager->createQuery('SELECT COUNT(t.twitterId) FROM SnowballFactory\TweetpostBundle\Entity\TwitterUser t')->getSingleScalarResult();

        $recentUsers = $entityManager->getRepository("SnowballFactoryTweetpostBundle:User")->findRecentlyLoggedIn(15);

        return array('jobsCount' => $jobsCount, 'usersCount' => $usersCount, 'twitterUsersCount' => $twitterUsersCount, 'recentUsers' => $recentUsers);
    }
}
