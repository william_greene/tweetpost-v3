<?php

namespace SnowballFactory\TweetpostBundle\Awesm;

class Awesm
{

    protected $key;

    public function __construct($options)
    {
        if(!isset($options['key']))
        {
            throw new \InvalidArgumentException('Awesm requires a key.');
        }

        $this->key = $options['key'];
    }

    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * Calls the url/retweet endpoint. Automatically parses URIs, replaces them with awe.sm tracking links, and returns all metadata
     *
     * @link http://developers.awe.sm/apis/url-retweet/
     */
    public function retweet($text, $url = null)
    {
        $params = array(
        	'v' => 3,
        	'key' => $this->key,
        	'tool' => 'jKbzg3',
        	'channel' => 'facebook-post',
        	'url' => $url,
        	'text' => $text,
        	'format' => 'json',
        	'unwrap' => 'true'
        );

        $request = curl_init();
        curl_setopt($request, CURLOPT_URL, 'http://api.awe.sm/url/retweet.json');
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_TIMEOUT, 10);
        curl_setopt($request, CURLOPT_POST, 1);
        curl_setopt($request, CURLOPT_POSTFIELDS, $params);

        $response = curl_exec($request);
        $response = json_decode($response, true);
        $responseCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
        curl_close($request);

        if ($responseCode != 200) {
            $error = print_r($response, true);

            throw new \Exception("Awe.sm API Retweet error: HTTP {$responseCode}:".$error);
        }

        return $response;
    }

    /**
     * Calls the url/update endpoint for information about the post to Facebook
     *
     * @link http://developers.awe.sm/apis/url-update-awesm_id/
     */
    public function update($awesmId, $facebookPostId, $facebookUserId, $facebookSharedAt)
    {
        $params = array(
        	'v' => 3,
        	'key' => $this->key,
        	'format' => 'json',
        	'tool' => 'jKbzg3',
        	'service_postid' => 'facebook:'.$facebookPostId,
        	'service_userid' => 'facebook:'.$facebookUserId,
        	'service_postid_shared_at' => $facebookSharedAt,
        	'service_postid_reach' => '1',
        	'channel' => 'facebook-post'
        );

        $request = curl_init();
        curl_setopt($request, CURLOPT_URL, 'http://api.awe.sm/url/update/'.$awesmId.'.json');
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_TIMEOUT, 10);
        curl_setopt($request, CURLOPT_POST, 1);
        curl_setopt($request, CURLOPT_POSTFIELDS, $params);

        $response = curl_exec($request);
        $response = json_decode($response, true);
        $responseCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
        curl_close($request);

        if ($responseCode != 200) {
            $error = print_r($response, true);

            throw new \Exception("Awe.sm API Update error: HTTP {$responseCode}:".$error);
        }

        return $response;
    }

}
