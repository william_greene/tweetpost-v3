<?php

namespace SnowballFactory\TweetpostBundle\Security\Authentication\User\Provider;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Validator\Validator;

use FOS\UserBundle\Model\UserManager;

use FOS\FacebookBundle\Security\Authentication\Token\FacebookUserToken;

use SnowballFactory\TweetpostBundle\Entity\User;

use \BaseFacebook;
use \FacebookApiException;

class FacebookProvider implements AuthenticationProviderInterface, UserProviderInterface, UserCheckerInterface
{

    protected $facebook;
    protected $userManager;
    protected $container;

    public function __construct(BaseFacebook $facebook, UserManager $userManager, Validator $validator, Container $container)
    {
        $this->facebook = $facebook;
        $this->userManager = $userManager;
        $this->validator = $validator;
        $this->container = $container;
    }

    public function authenticate(TokenInterface $token)
    {
        if (!$this->supports($token)) {
            return null;
        }

        try {
            if ($uid = $this->facebook->getUser()) {
                return $this->createAuthenticatedToken($uid);
            }
        } catch (\Exception $failed) {
            throw new AuthenticationException('Unknown error'.$failed->getMessage(), $failed->getMessage(), $failed->getCode(), $failed);
        }

        throw new AuthenticationException('The Facebook user could not be retrieved from the session.');
    }

    public function supports(TokenInterface $token)
    {
        return $token instanceof FacebookUserToken;
    }

    protected function createAuthenticatedToken($uid)
    {
        if (null === $this->userManager) {
            return new FacebookUserToken($uid);
        }

        $user = $this->loadUserByUsername($uid);
        if (!$user instanceof UserInterface) {
            throw new \RuntimeException('User provider did not return an implementation of user interface.');
        }

        $this->checkPreAuth($user);
        $this->checkPostAuth($user);

        return new FacebookUserToken($user, $user->getRoles());
    }

    public function supportsClass($class)
    {
        return $class === $this->getClass();
    }

    public function findUserByFacebookId($facebookId)
    {
        return $this->userManager->findUserBy(array('facebookId' => $facebookId));
    }

    public function loadUserByUsername($username)
    {
        $user = $this->findUserByFacebookId($username);

        try {
            $data = $this->facebook->api('/me');
        } catch (FacebookApiException $e) {
            $data = null;
        }

        if (!empty($data)) {
            if (empty($user)) {
                $user = $this->userManager->createUser();
                $user->setEnabled(true);
                $user->setPassword($this->container->get('security.encoder_factory')->getEncoder($user)->encodePassword('tweetpost', $user->getSalt()));
            }

            $data['facebook_access_token'] = $this->facebook->getAccessToken();
            $user->setFacebookData($data);

            if (count($this->validator->validate($user, 'Facebook'))) {
                throw new UsernameNotFoundException('The facebook user could not be stored');
            }
            $this->userManager->updateUser($user);
        }

        if (empty($user)) {
            throw new UsernameNotFoundException('The user is not authenticated on facebook');
        }

        return $user;
    }

    /**
     * Refresh a user
     *
     * @param UserInterface $user
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$this->supportsClass(get_class($user)) || !$user->getFacebookId()) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getFacebookId());
    }

    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException('User is not supported.');
        }
    }

    public function checkPostAuth(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException('User is not supported.');
        }
    }
}
