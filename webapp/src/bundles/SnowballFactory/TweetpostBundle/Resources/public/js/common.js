
/*
  Setup user voice code
 */
(function() {
    var uv = document.createElement('script');
    uv.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.uservoice.com/javascripts/widgets/tab.js';
    uv.async = true;
    document.getElementById('tweetpost-body').appendChild(uv);
})();

/*
  Facebook Auth Callback
 */
function login() {
    window.location.href = "/user/facebook/authorize";
}

function init() {
  if (typeof(FB) != 'undefined' && FB != null ) {

      FB.Event.subscribe('auth.login', function(response) {
          if (response.authResponse) {
              setTimeout(login, 500);
          }
      });

  }
}

YUI({ filter: 'min', debug: false }).use('node', 'event', function (Y) {

    Y.on('domready', function() {

        /*
          Setup user voice tab
         */
        UserVoice.Tab.show({ key: 'tweetpost', host: 'tweetpost.uservoice.com', lang: 'en', forum: 'general', alignment: 'left', background_color:'#81BEF7', text_color: 'white', hover_color: '#000'});

       /**
        * Connections UI Management
        */
        var connection = Y.one('#bd-connections #connection_name');
        if (connection) {

            /* default states */
        	 if(Y.one('#connection_awesmTracking[type=checkbox][checked]')) {
        		 Y.one('#connection-awesm-key').toggleClass('hidden');
        	 }

           /* toggle awesm key */
           Y.one('#connection_awesmTracking').on('click', function(e) {
              Y.one('#connection-awesm-key').toggleClass('hidden');
           });

           /* sync connection name */
           Y.all('#bd-connections select').on('change', function() {

              var twitter = Y.one("select#connection_twitter");
              var facebook = Y.one("select#connection_facebookId");

              connection.set('value', twitter.get("options").item(twitter.get('selectedIndex')).get('text') + " to " + facebook.get("options").item(facebook.get('selectedIndex')).get('text'));

           });

        }

    });
});
