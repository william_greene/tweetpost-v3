<?php

namespace SnowballFactory\TweetpostBundle\Engine;

use \Phirehose;
use \OauthPhirehose;

use \Resque;

/**
 *
 */
class Consumer extends OauthPhirehose
{

  /**
   * Overidden constructor to take class-specific parameters
   *
   * @param string $username
   * @param string $password
   */
  public function __construct($container, $options = array())
  {
      $this->container = $container;

      $this->consumerKey = $this->container->getParameter('twitter.oauth.key');
      $this->consumerSecret = $this->container->getParameter('twitter.oauth.secret');

  	  Resque::setBackend($this->container->getParameter('cache.host').':'.$this->container->getParameter('cache.port'));

      parent::__construct($this->container->getParameter('twitter.oauth.token'), $this->container->getParameter('twitter.oauth.token.secret'), Phirehose::METHOD_FILTER, Phirehose::FORMAT_JSON);

      $this->setFollow($this->getFollowIds($this->container->getParameter('snowball_factory_tweetpost.follow_limit')));
  }

  public function getContainer()
  {
    return $this->container;
  }

  public function checkFilterPredicates()
  {
      $this->setFollow($this->getFollowIds($this->container->getParameter('snowball_factory_tweetpost.follow_limit')));
  }

  protected function getFollowIds($limit = 100000)
  {
      $twitterUsers = $this->getContainer()->get('doctrine.orm.entity_manager')->createQuery('SELECT DISTINCT t.twitterId FROM SnowballFactory\TweetpostBundle\Entity\TwitterUser t WHERE t.polling=true ORDER BY t.updatedAt ASC')->setMaxResults($limit)->getResult();

      $follows = array();
      foreach($twitterUsers as $twitterUser)
      {
          $follows[] = $twitterUser['twitterId'];
      }

      return $follows;
  }

  /**
   * Enqueue each status
   *
   * @param string $status
   */
  public function enqueueStatus($status)
  {
    $data = array('tweet' => json_decode($status));

    $token = Resque::enqueue('tweetpost_tweets', '\SnowballFactory\TweetpostBundle\Engine\Publisher', $data, true);

    return $token;
  }

  public function log($message, $severity = 'notice')
  {
      echo $message."\n";
  }

}
