<?php

namespace SnowballFactory\TweetpostBundle\Engine;

use \Doctrine\ORM\EntityManager;

use \TwitterOAuth;
use \BaseFacebook;
use \Embedly\Embedly;
use \SnowballFactory\TweetpostBundle\Awesm\Awesm;

use \SnowballFactory\TweetpostBundle\Entity\Tweet;

use \Resque;

use \DateTime;

use \Exception;

class Processor
{
    protected $entityManager;
    protected $facebook;
    protected $twitter;
    protected $embedly;
    protected $awesm;

    protected $options;

    /**
    * Construct the consumer and start processing
    */
    public function __construct(EntityManager $entityManager, BaseFacebook $facebook, TwitterOAuth $twitter, Embedly $embedly, Awesm $awesm, array $options = array())
    {
        $this->entityManager = $entityManager;
        $this->facebook = $facebook;
        $this->twitter = $twitter;
        $this->embedly = $embedly;
        $this->awesm = $awesm;
        $this->options = $options;
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }

    public function getFacebook()
    {
        return $this->facebook;
    }

    public function getEmbedly()
    {
        return $this->embedly;
    }

    public function getAwesm()
    {
        return $this->awesm;
    }

    public function process($job)
    {
        try {

            // check for different events from stream
            if(isset($job['tweet']['delete']))
            {
                return $this->delete($job['tweet']['delete']);
            }
            elseif(isset($job['tweet']['scrub_geo']))
            {
                return $this->scrub($job['tweet']['scrub_geo']);
            }
            elseif(isset($job['tweet']['limit']))
            {
                return $this->limit($job['tweet']['limit']);
            }
            elseif(isset($job['tweet']))
            {
                return $this->post($job['tweet']);
            }
        }
        catch(\Exception $e)
        {
            $job['exception'] = $e->getMessage();
            $job['failed_attempts'] = isset($job['failed_attempts']) ? ($job['failed_attempts'] + 1) : 1;

            if($job['failed_attempts'] <= 2)
            {
                $token = Resque::enqueue('tweetpost_tweets', '\SnowballFactory\TweetpostBundle\Engine\Publisher', $job, true);
            }

            throw $e;
        }
    }

    public function delete($event)
    {
        if(isset($event['status']['id_str']))
        {
            $tweets = $this->getEntityManager()->createQuery('SELECT t FROM SnowballFactory\TweetpostBundle\Entity\Tweet t WHERE t.tweetId = :id ORDER BY t.updatedAt ASC')->setParameter('id', $event['status']['id_str'])->getResult();
            foreach($tweets as $tweet)
            {
                // force use of application access token
                $this->getFacebook()->api('/'.$tweet->getFacebookPostId(), 'DELETE', array('access_token' => $this->getFacebook()->getAppId().'|'.$this->getFacebook()->getAppSecret()));
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public function scrub($event)
    {
        // ignore event

        return true;
    }

    public function limit($event)
    {
        // ignore event

        return true;
    }

    public function post($tweet)
    {
        try {

            var_dump($tweet);

            $posts = $this->publish($tweet);

            var_dump($posts);
        }
        catch(\Exception $e)
        {
            // catch oauth failure - throw exception to requeue

            error_log($e->getMessage());

            throw $e;
        }
    }

    public function publish($tweet)
    {
        $posts = array();

        // ignore if tweet includes !fb or from facebook
        if((strpos($tweet['text'], '!fb') !== false) || (strpos($tweet['source'], 'Facebook') !== false))
        {
            return;
        }

        // ignore replies unless #fb
        if((strpos($tweet['text'], '#fb') === false) && ((isset($tweet['in_reply_to_status_id']) && !empty($tweet['in_reply_to_status_id'])) || (isset($tweet['in_reply_to_user_id']) && !empty($tweet['in_reply_to_user_id']))))
        {
            return;
        }

        // twitter user id
        $twitterUserId = $tweet['user']['id_str'];

        // find active connections related to tweet user id
        $connections = $this->getEntityManager()->createQuery('SELECT c, t FROM SnowballFactory\TweetpostBundle\Entity\Connection c LEFT JOIN c.twitter t WHERE c.active = true AND t.twitterId = :id ORDER BY t.updatedAt ASC')->setParameter('id', $twitterUserId)->getResult();
        foreach($connections as $connection)
        {
            // fb post
            $post = array();

            // default to connection token, if none set use the user connection (allows for fb token reset + background task to update fb app/page tokens - once user is valid)
            $post['access_token'] = $connection->getAccessToken() ? $connection->getAccessToken() : $connection->getUser()->getFacebookAccessToken();

            // set facebook api token for user/page/app
            $this->getFacebook()->setAccessToken($post['access_token']);

            // default message = tweet text
            $post['message'] = $tweet['text'];

            // skip if filter on hashtag (and not #fb)
            if(($connection->getFilterHashtag() === true) && (strpos($tweet['text'], '#fb') === false))
            {
                continue;
            }

            // check blacklist for source (and not #fb)
            if(in_array($tweet['source'], $connection->getBlacklist()) && (strpos($tweet['text'], '#fb') === false))
            {
                continue;
            }

            // check if retweet
            if(isset($tweet['retweeted_status'], $tweet['retweeted_status']['created_at'], $tweet['retweeted_status']['text']))
            {
                // skip if retweet disabled (and not #fb)
                if(($connection->getDisableRetweet() === true) && (strpos($tweet['text'], '#fb') === false))
                {
                    continue;
                }

                // if allowed rewrite name with /via
                $name = $connection->getConvertNames() ? $tweet['retweeted_status']['user']['name'] : $tweet['retweeted_status']['user']['screen_name'];
                $post['message'] = $tweet['retweeted_status']['text'] . ' /via '.$name;
            }

            // check add action link option
            if(($connection->getActionLink() === true))
            {
                $post['actions'] = array();
                $post['actions'][] = array('name' => 'Follow me on Twitter', 'link' => 'http://twitter.com/'.$connection->getTwitter()->getUsername());
            }

            // check if twitter has media entities
            if(isset($tweet['entities']['media']) && (count($tweet['entities']['media']) > 0))
            {
                // add native pictures
                if($tweet['entities']['media'][0]->type == 'photo')
                {
                    $post['type'] = 'photo';
                    $post['picture'] = $tweet['entities']['media'][0]->media_url;
                }
            }

            // check any links if option enabled
            if(($connection->getPostLinks() === true) && (count($tweet['entities']['urls']) > 0))
            {
                // default meta data
                $post['type'] = 'link';
                $post['link'] = $tweet['entities']['urls'][0]['url'];

                // embedly objectify call
                $preview = $this->embedly->objectify(array('url' => $tweet['entities']['urls'][0]['url']));
                if(!empty($preview))
                {
                    $post['link'] = $preview[0]->url;

                    // default embedly data
                    if(isset($preview[0]->title))
                    {
                        $post['name'] = $preview[0]->title;
                        $post['description'] = isset($preview[0]->description) ? $preview[0]->description : null;
                    }

                    // oembed
                    if(isset($preview[0]->oembed) && isset($preview[0]->oembed->title))
                    {
                        $post['name'] = isset($preview[0]->oembed->title) ? $preview[0]->oembed->title : $preview[0]->title;
                        $post['description'] = isset($preview[0]->oembed->description) ? $preview[0]->oembed->description : $preview[0]->description;

                        // oembed thumbnail
                        if(isset($preview[0]->oembed->thumbnail_url))
                        {
                            $post['picture'] = $preview[0]->oembed->thumbnail_url;
                            $post['type'] = 'photo';
                        }
                    }

                    // open graph data
                    if(isset($preview[0]->open_graph) && isset($preview[0]->open_graph->title))
                    {
                        $post['name'] = isset($preview[0]->open_graph->title) ? $preview[0]->open_graph->title : $preview[0]->title;
                        $post['description'] = isset($preview[0]->open_graph->description) ? $preview[0]->open_graph->description : $preview[0]->description;
                        $post['link'] = isset($preview[0]->open_graph->url) ? $preview[0]->open_graph->url : $preview[0]->url;
                        $post['caption'] = $tweet['entities']['urls'][0]['display_url'];

                        // embedly picture
                        if(isset($preview[0]->images) && !empty($preview[0]->images))
                        {
                            $post['picture'] = $preview[0]->images[0]->url;
                            $post['type'] = 'photo';
                        }

                    }

                    // oembed media
                    if(isset($preview[0]->oembed))
                    {

                        // oembed photo post
                        if(isset($preview[0]->oembed->type) && $preview[0]->oembed->type == 'photo')
                        {
                            $post['type'] = 'photo';
                            if(isset($preview[0]->oembed->url))
                            {
                                $post['source'] = $preview[0]->oembed->url;
                            }
                        }

                        // oembed video post
                        if(isset($preview[0]->oembed->type) && $preview[0]->oembed->type == 'video')
                        {
                            $post['type'] = 'video';
                            if(isset($preview[0]->oembed->url))
                            {
                                $post['source'] = $preview[0]->oembed->url;
                            }
                        }
                    }

                    // opengraph media
                    if(isset($preview[0]->open_graph))
                    {
                        // opengraph photo post
                        if(isset($preview[0]->open_graph->image))
                        {
                            $post['type'] = 'photo';
                            $post['source'] = $preview[0]->open_graph->image;
                        }

                        // opengraph video post
                        if(isset($preview[0]->open_graph->video))
                        {
                            $post['type'] = 'video';
                            $post['source'] = $preview[0]->open_graph->video;
                        }
                    }

                }

                // awesm instrumentation
                if(($connection->getAwesmTracking() === true) && ($connection->getAwesmApiKey() !== null))
                {
                    try {
                        $this->awesm->setKey($connection->getAwesmApiKey());

                        $awesm = $this->awesm->retweet($tweet['text'], $post['link']);

                        $post['message'] = $awesm['awesm_text'];
                        $post['link'] = $awesm['awesm_urls'][0]['awesm_url'];
                        $post['awesm_id'] = $awesm['awesm_urls'][0]['awesm_id'];
                    }
                    catch(Exception $e)
                    {
                        // catch and ignore awesm retweet exceptions
                        error_log($e->getMessage());
                    }
                }

            }

            // convert twitter names
            if(($connection->getConvertNames() === true) && (count($tweet['entities']['user_mentions']) > 0))
            {
                foreach($tweet['entities']['user_mentions'] as $mention)
                {
                    $post['message'] = str_replace('@'.$mention['screen_name'], $mention['name'], $post['message']);
                }
            }

            // facebook api call, let exception bubble up and requeue if ratelimited or failed
            $facebookResponse = $this->getFacebook()->api('/'.$connection->getFacebookId().'/feed', 'POST', $post);
            $post['id'] = $facebookResponse['id'];

            // awesm api call, catch and log exceptions (as we have post to facebook)
            if(isset($awesm) && isset($awesm['awesm_urls']))
            {
                try {
                    $date = new \DateTime();
                    $awesm = $this->awesm->update($awesm['awesm_urls'][0]['awesm_id'], $post['id'], $connection->getFacebookId(), $date->format('Y-m-d\TH:i:sO'));
                }
                catch(Exception $e)
                {
                    // catch and ignore awesm update exceptions
                    error_log($e->getMessage());
                }
            }

            // save tweet + facebook post
            $archive = new Tweet();
            $archive->setConnection($connection);
            $archive->setTweetId($tweet['id_str']);
            $archive->setTweetUserId($tweet['user']['id_str']);
            $archive->setFacebookPostId($post['id']);
            $archive->setFacebookUserId($connection->getFacebookId());
            $archive->setData(array('tweet' => $tweet, 'post' => $post));
            $archive->setStatus('published');

            $this->entityManager->persist($archive);
            $this->entityManager->flush();

            // return final fb/awesm/embedly post data
            $posts[] = $post;
        }

        // return posts or false for no posts
        return empty($posts) ? false : $posts;
    }

}
