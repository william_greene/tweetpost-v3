<?php

namespace SnowballFactory\TweetpostBundle\Engine;

require_once __DIR__.'/../../../../../tweetpost/autoload.php';
require_once __DIR__.'/../../../../../tweetpost/TweetpostKernel.php';

use \TweetpostKernel;

class Publisher
{

  static protected $kernel = null;

  public $args = array();

  public function setUp()
  {
    if(!isset(static::$kernel))
    {
      static::$kernel = new TweetpostKernel(getenv('SYMFONY__ENV') ?: 'dev', !getenv('SYMFONY__DEBUG'));
      static::$kernel->boot();
    }
  }

  public function tearDown()
  {
      if (null !== static::$kernel) {
          static::$kernel->shutdown();
      }
  }

  public function perform()
  {
      return static::$kernel->getContainer()->get('snowball_factory_tweetpost.engine.processor')->process($this->args);
  }

}
