<?php

namespace SnowballFactory\TweetpostBundle\Api;

use Symfony\Component\HttpFoundation\Session\Session;

use FOS\FacebookBundle\Facebook\FacebookSessionPersistence;
use FacebookApiException;

use Predis\Client;

class Facebook extends FacebookSessionPersistence
{
    public static $CURL_OPTS = array(
        CURLOPT_CONNECTTIMEOUT => 3,
        CURLOPT_TIMEOUT        => 10,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_USERAGENT      => 'Tweetpo.st v2',
    );

    protected $session = null;
    protected $cache = null;

    public function __construct($config, Session $session, Client $cache)
    {
        $this->session = $session;
        $this->cache = $cache;

        parent::__construct($config, $session, '_fb_');

        $this->setFileUploadSupport(false);
    }

    public function getAccountChoices()
    {
        $key = 'facebook_choices_'.md5($this->getAccessToken());
        $choices = unserialize($this->cache->get($key));
        if(empty($choices))
        {
            $profile  = $this->api('/me');
            $accounts = $this->api('/me/accounts');

            if(!isset($profile['username']))
            {
                $profile['username'] = $profile['first_name'] . ' ' .$profile['last_name'];
            }

            $choices = array($profile['id'] => $profile['username'] . ' (Profile)');
            if(isset($accounts['data']))
            {
                foreach($accounts['data'] as $account)
                {
                    $id = $account['id'];

                    if($account['category'] == 'Application' && isset($account['name']))
                    {
                        $choices[$id] = $account['name'] . ' (App)';
                    }
                    else if(isset($account['name']))
                    {
                        $choices[$id] = $account['name'] . ' (Page)';
                    }
                }

                $this->cache->set($key, serialize($choices));
                $this->cache->expire($key, 600);
            }
        }

        return $choices;
    }
}
