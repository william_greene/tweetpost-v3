<?php

namespace SnowballFactory\TweetpostBundle\Api;

use TwitterOAuth as BaseTwitter;

use Predis\Client;

class Twitter extends BaseTwitter
{
    protected $cache = null;

    public $connecttimeout = 3;
    public $timeout = 10;
    public $format = 'json';
    public $decode_json = true;
    public $useragent = 'Tweetpo.st v2';
    public $ssl_verifypeer = false;
    public $retry = true;

    public function __construct($consumer_key, $consumer_secret, $oauth_token = NULL, $oauth_token_secret = NULL, Client $cache)
    {
        $this->cache = $cache;

        parent::__construct($consumer_key, $consumer_secret, $oauth_token, $oauth_token_secret);
    }

    public function getBlacklistChoices($count = 100)
    {
        $key = 'twitter_blacklist_'.md5($this->token->__toString().$count);
        $sources = unserialize($this->cache->get($key));
        if(empty($sources))
        {
            $sources = array('web' => '<a href="http://twitter.com/" rel="nofollow">Twitter.com</a>');

            $data = $this->get('statuses/user_timeline', array('count' => $count));
            if (!empty($data) && is_array($data))
            {
                foreach ($data as $tweet)
                {
                    if (!in_array($tweet->source, $sources) && ($tweet->source !== 'web'))
                    {
                        $sources[$tweet->source] = $tweet->source;
                    }
                }

                $this->cache->set($key, serialize($sources));
                $this->cache->expire($key, 600);
            }
        }

        return $sources;
    }

}
