<?php

namespace SnowballFactory\TweetpostBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('snowball_factory_tweetpost');
        $rootNode
            ->children()
                ->scalarNode('facebook_key')->end()
                ->scalarNode('facebook_secret')->end()

                ->scalarNode('twitter_username')->end()
                ->scalarNode('twitter_password')->end()
                ->scalarNode('twitter_key')->end()
                ->scalarNode('twitter_secret')->end()
                ->scalarNode('twitter_access_token')->end()
                ->scalarNode('twitter_access_token_secret')->end()

                ->scalarNode('awesm_key')->end()

                ->scalarNode('embedly_key')->end()

                ->scalarNode('uservoice_key')->end()

                ->scalarNode('follow_limit')->defaultValue(10000)->end()
            ->end();

        return $treeBuilder;
    }
}
