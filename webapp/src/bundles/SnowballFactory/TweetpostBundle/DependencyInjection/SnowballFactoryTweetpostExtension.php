<?php

namespace SnowballFactory\TweetpostBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Config\FileLocator;

class SnowballFactoryTweetpostExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        $container->setParameter('snowball_factory_tweetpost.twitter_username', $config['twitter_username']);
        $container->setParameter('snowball_factory_tweetpost.twitter_password', $config['twitter_password']);
        $container->setParameter('snowball_factory_tweetpost.twitter_key', $config['twitter_key']);
        $container->setParameter('snowball_factory_tweetpost.twitter_secret', $config['twitter_secret']);
        $container->setParameter('snowball_factory_tweetpost.twitter_access_token', $config['twitter_access_token']);
        $container->setParameter('snowball_factory_tweetpost.twitter_access_token_secret', $config['twitter_access_token_secret']);

        $container->setParameter('snowball_factory_tweetpost.facebook_key', $config['facebook_key']);
        $container->setParameter('snowball_factory_tweetpost.facebook_secret', $config['facebook_secret']);

        $container->setParameter('snowball_factory_tweetpost.awesm_key', $config['awesm_key']);

        $container->setParameter('snowball_factory_tweetpost.embedly_key', $config['embedly_key']);

        $container->setParameter('snowball_factory_tweetpost.uservoice_key', $config['uservoice_key']);

        $container->setParameter('snowball_factory_tweetpost.follow_limit', $config['follow_limit']);
    }

    public function getAlias()
    {
        return 'snowball_factory_tweetpost';
    }
}
