<?php

namespace SnowballFactory\TweetpostBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ConnectionControllerTest extends WebTestCase
{
    public function testDashboard()
    {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/connection/dashboard');

        $client->followRedirect();

        $this->assertTrue($client->getResponse()->getContent(), 'http://localhost/user/login');
    }
}
