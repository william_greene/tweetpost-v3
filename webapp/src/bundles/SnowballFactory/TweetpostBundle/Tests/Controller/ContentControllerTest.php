<?php

namespace SnowballFactory\TweetpostBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ContentControllerTest extends WebTestCase
{
    private function checkCommonElements($client, $crawler)
    {
        $this->assertTrue($client->getResponse()->isSuccessful());
        
        $this->assertFalse($client->getResponse()->isCacheable());
        
        $this->assertEquals('text/html; charset=UTF-8', $client->getResponse()->headers->get('Content-Type'));
        
        $this->assertTrue($crawler->filter('link[rel="stylesheet"]')->count() === 2);
        $this->assertTrue($crawler->filter('script[type="text/javascript"]')->count() === 2);    

        $this->assertTrue($crawler->filter('title:contains("TweetPo.st")')->count() > 0);
    }
    
    public function testHomepage()
    {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/');
        
        $this->checkCommonElements($client, $crawler);

        $this->assertTrue($crawler->filter('title:contains("TweetPo.st | A smarter way to connect Twitter and Facebook")')->count() > 0);
    }
    
    public function testPrivacy()
    {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/privacy-policy');

        $this->checkCommonElements($client, $crawler);

        $this->assertTrue($crawler->filter('title:contains("TweetPo.st | Privacy Policy")')->count() > 0);
    }
    
    public function testTos()
    {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/terms-of-service');

        $this->checkCommonElements($client, $crawler);
        
        $this->assertTrue($crawler->filter('title:contains("TweetPo.st | Terms of Service")')->count() > 0);
    }

    public function testErrorNotFound()
    {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/error-not-found');
        
        $this->assertTrue($client->getResponse()->isNotFound());
        $this->assertFalse($client->getResponse()->isCacheable());
        
        $this->assertEquals('text/html; charset=UTF-8', $client->getResponse()->headers->get('Content-Type'));
    }
}
