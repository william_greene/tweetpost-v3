<?php

namespace SnowballFactory\TweetpostBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    public function testLogin()
    {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/user/login');

        $this->assertTrue($client->getResponse()->isSuccessful());

        // display form
        $this->assertEquals('text/html; charset=UTF-8', $client->getResponse()->headers->get('Content-Type'));
        $this->assertTrue($crawler->filter('html:contains("TweetPo.st")')->count() > 0);
        $this->assertTrue($crawler->filter('body:contains("Sign in to TweetPo.st")')->count() > 0);
        $this->assertTrue($crawler->filter('form:contains("Username")')->count() > 0);
        $this->assertTrue($crawler->filter('form:contains("Password")')->count() > 0);

        // failure
        $crawler = $client->submit($crawler->selectButton('login')->form(), array('_username' => 'dustinwhittle', '_password' => 'tweetpost'));
        // $crawler = $client->followRedirect();

        // $this->assertTrue($client->getResponse()->isSuccessful());
        // $this->assertTrue($crawler->filter('div[id="notifications"]:contains("Bad credentials")')->count() > 0);

        // success
        // $crawler = $client->submit($crawler->selectButton('login')->form(), array('_username' => 'dustinwhittle', '_password' => 'tweetpost'));
    }

    public function testLogout()
    {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/user/logout');
    }

    public function testInfo()
    {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/user/info');

        $this->assertTrue($client->getResponse()->isSuccessful());

        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
        $this->assertEquals('[]', $client->getResponse()->getContent());
    }
}
