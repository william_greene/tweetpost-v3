<?php

namespace SnowballFactory\TweetpostBundle\Tests\Command;

require_once __DIR__.'/../../../../../../tweetpost/TweetpostKernel.php';

use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Console\Tester\ApplicationTester;

use Symfony\Bundle\FrameworkBundle\Console\Application;

use SnowballFactory\TweetpostBundle\Command\TweetpostConsumerCommand;

use \TweetpostKernel;

class TweetpostConsumerCommandTest extends \PHPUnit_Framework_TestCase
{
    public function testExecute()
    {
        $kernel = new TweetpostKernel('test', true);
        $application = new Application($kernel);
        $command = $application->find('tweetpost:consumer');
        
        $this->assertEquals('tweetpost', $command->getNamespace(), '->getNamespace() returns the command namespace');
        $this->assertEquals('consumer', $command->getName(), '->getName() returns the command name');
        $this->assertEquals('tweetpost:consumer', $command->getFullName(), '->getNamespace() returns the full command name');

        $commandTester = new CommandTester($command);
        $commandTester->execute(array());
        $this->assertRegExp('/Consuming/', $commandTester->getDisplay(), '->execute() returns a log of consumer jobs');
    }
}
