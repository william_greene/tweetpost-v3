<?php

namespace SnowballFactory\TweetpostBundle\Tests\Command;

require_once __DIR__.'/../../../../../../tweetpost/TweetpostKernel.php';

use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Console\Tester\ApplicationTester;

use Symfony\Bundle\FrameworkBundle\Console\Application;

use SnowballFactory\TweetpostBundle\Command\TweetpostPublisherCommand;

use \TweetpostKernel;

class TweetpostPublisherCommandTest extends \PHPUnit_Framework_TestCase
{
    public function testExecute()
    {
        $kernel = new TweetpostKernel('test', true);
        $application = new Application($kernel);
        $command = $application->find('tweetpost:publisher');

        $this->assertEquals('tweetpost', $command->getNamespace(), '->getNamespace() returns the command namespace');
        $this->assertEquals('publisher', $command->getName(), '->getName() returns the command name');
        $this->assertEquals('tweetpost:publisher', $command->getFullName(), '->getNamespace() returns the full command name');

        $commandTester = new CommandTester($command);
        $commandTester->execute(array());
        $this->assertRegExp('/Publishing/', $commandTester->getDisplay(), '->execute() returns a log of publishing jobs');
    }
}
