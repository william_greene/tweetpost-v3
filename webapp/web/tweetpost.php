<?php

require_once __DIR__.'/../tweetpost/autoload.php';
require_once __DIR__.'/../tweetpost/TweetpostCache.php';
require_once __DIR__.'/../tweetpost/TweetpostKernel.php';

use Symfony\Component\HttpFoundation\Request;

$kernel = new TweetpostKernel('prod', false);
$kernel->loadClassCache();
$kernel->handle(Request::createFromGlobals())->send();
