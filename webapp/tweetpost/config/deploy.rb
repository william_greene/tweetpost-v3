
set :application, "tweetpo.st"
set :domain,      "#{application}"
set :deploy_to,   "/var/www/#{domain}"

set :app_path, "tweetpost"
set :web_path, "web"

set :symfony_console,     app_path + "/console"
set :log_path,            app_path + "/logs"
set :cache_path,          app_path + "/cache"

set :serverName, "184.72.164.58"
set :user,       "tweetpost"

ssh_options[:port] = 22
# ssh_options[:forward_agent] = true
# ssh_options[:keys] = [app_path + "/config/aws/keys/tweetpost-ec2-us-west2.pem"]

set :repository,  "git@github.com:awesm/tweetpost-v3.git"
set :scm,         :git
set :branch,      "master"
set :scm_verbose, true
set :deploy_via,  :rsync_with_remote_cache

set :model_manager, "doctrine"

role :web,        serverName
role :app,        serverName
role :db,         serverName, :primary => true

set :keep_releases,  1
set :use_sudo,       false

set :git_enable_submodules, true
set :git_shallow_clone, true

set :update_vendors, false
set :cache_warmup, true

set :assets_install, true
set :dump_assetic_assets, true

set :shared_files,    [app_path + "/config/nginx.conf", app_path + "/config/php.conf", app_path + "/config/parameters.yml"]
set :shared_children, [app_path + "/logs", app_path + "/cache", web_path + "/uploads", app_path + "/src/vendor"]
set :asset_children,  [web_path + "/css", web_path + "/images", web_path + "/js", web_path + "/bundles"]


after "deploy:cold" do
    run "sudo ln -s /var/www/tweetpo.st/current/webapp/tweetpost/config/nginx.conf  /etc/nginx/sites-available/tweetpo.st ; sudo ln -s /etc/nginx/sites-available/tweetpo.st /etc/nginx/sites-enabled/tweetpo.st"
    run "sudo ln -s /var/www/tweetpo.st/current/webapp/tweetpost/config/php.conf  /etc/php5/conf.d/tweetpost.ini"
end

after "deploy:restart" do
    run "sudo chmod -R 777 /var/www/tweetpo.st/releases/*/webapp/tweetpost/cache ; sudo chmod -R 777 /var/www/tweetpo.st/releases/*/webapp/tweetpost/logs"
    run "sudo service nginx restart ; sudo service php5-fpm restart"
end

after "deploy:finalize_update" do
  deploy.cleanup
end
