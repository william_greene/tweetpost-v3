

# always use utf8 + utc for timezone
SET names utf8;
SET SESSION time_zone = '+0:00';


# databases (tweetpost + tweetpost_sessions)
CREATE DATABASE IF NOT EXISTS tweetpost
	CHARACTER SET utf8
	DEFAULT CHARACTER SET utf8
	COLLATE utf8_unicode_ci
	DEFAULT COLLATE utf8_unicode_ci;

CREATE DATABASE IF NOT EXISTS tweetpost_development
	CHARACTER SET utf8
	DEFAULT CHARACTER SET utf8
	COLLATE utf8_unicode_ci
	DEFAULT COLLATE utf8_unicode_ci;

DROP DATABASE IF EXISTS tweetpost_test;
CREATE DATABASE IF NOT EXISTS tweetpost_test
	CHARACTER SET utf8
	DEFAULT CHARACTER SET utf8
	COLLATE utf8_unicode_ci
	DEFAULT COLLATE utf8_unicode_ci;


# users
GRANT all privileges on tweetpost.* to tweetpost@localhost identified by 'tw33tFence';
GRANT all privileges on tweetpost_development.* to tweetpost_dev@localhost identified by 'tw33tFence_development';
GRANT all privileges on tweetpost_test.* to tweetpost_dev@localhost identified by 'tw33tFence_development';

# flush
FLUSH PRIVILEGES;
