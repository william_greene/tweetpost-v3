<?php

require_once __DIR__.'/../src/vendor/symfony/src/Symfony/Component/ClassLoader/UniversalClassLoader.php';
require_once __DIR__.'/../src/vendor/symfony/src/Symfony/Component/ClassLoader/ApcUniversalClassLoader.php';

use Symfony\Component\ClassLoader\ApcUniversalClassLoader;
use Doctrine\Common\Annotations\AnnotationRegistry;

$loader = new ApcUniversalClassLoader('tweetpost.autoloader.');
$loader->registerNamespaces(array(
    'Symfony'                              => array(__DIR__.'/../src/vendor/symfony/src', __DIR__.'/../src/vendor/bundles'),

    'Doctrine\Bundle'                      => __DIR__.'/../src/vendor/bundles',
    'Doctrine\Common\DataFixtures'         => __DIR__.'/../src/vendor/doctrine-fixtures/lib ',
    'Doctrine\Common'                      => __DIR__.'/../src/vendor/doctrine-common/lib',
    'Doctrine\DBAL\Migrations'             => __DIR__.'/../src/vendor/doctrine-migrations/lib',
    'Doctrine\DBAL'                        => __DIR__.'/../src/vendor/doctrine-dbal/lib',
    'Doctrine\Tests'                       => __DIR__.'/../src/vendor/doctrine/tests',
    'Doctrine'                             => __DIR__.'/../src/vendor/doctrine/lib',

    'Propel'                               => __DIR__.'/../src/vendor/bundles',

    'Monolog'                              => __DIR__.'/../src/vendor/monolog/src',

    'Assetic'                              => __DIR__.'/../src/vendor/assetic/src',

    'Buzz'                                 => __DIR__.'/../src/vendor/buzz/lib',

    'Sensio'                               => __DIR__.'/../src/vendor/bundles',

    'JMS'                                  => __DIR__.'/../src/vendor/bundles',

    'Metadata'                             => __DIR__.'/../src/vendor/metadata/src',
    'CG'                                   => __DIR__.'/../src/vendor/cg-library/src',

    'Nelmio'                               => __DIR__.'/../src/vendor/bundles',
    'FOS'                                  => __DIR__.'/../src/vendor/bundles',

    'AntiMattr'                            => __DIR__.'/../src/vendor/bundles',

    'Imagine'                              => __DIR__.'/../src/vendor/imagine/lib',
    'Avalanche'                            => __DIR__.'/../src/vendor/bundles',

    'Snc'                                  => __DIR__.'/../src/vendor/bundles',
    'Predis'                               => __DIR__.'/../src/vendor/predis/lib',

    'Cybernox'                             => __DIR__.'/../src/vendor/bundles',

    'Embedly'                              => __DIR__.'/../src/vendor/embedly/src',

    'SnowballFactory'                      => __DIR__.'/../src/bundles',

));
$loader->registerPrefixes(array(
    'Twig_Extensions_'                     => __DIR__.'/../src/vendor/twig-extensions/lib',
    'Twig_'                                => __DIR__.'/../src/vendor/twig/lib',
    'Swift_'                               => __DIR__.'/../src/vendor/swiftmailer/lib/classes',
    'PHPParser'                            => __DIR__.'/../src/vendor/php-parser/lib',

    'Facebook'                             => __DIR__.'/../src/vendor/facebook/src',

    'Resque'                               => __DIR__.'/../src/vendor/resque/lib',

    'Twitter'                              => __DIR__.'/../src/vendor/twitter/twitteroauth',

    'Phirehose'                            => __DIR__.'/../src/vendor/phirehose/lib',
    'OauthPhirehose'                       => __DIR__.'/../src/vendor/phirehose/lib',

));


$loader->registerPrefixFallbacks(array(
    __DIR__.'/../src/vendor/symfony/src/Symfony/Component/HttpFoundation/Resources/stubs',
    __DIR__.'/../src/vendor/symfony/src/Symfony/Component/Locale/Resources/stubs',
));

$loader->registerNamespaceFallbacks(array(
    __DIR__.'/../src',
));
$loader->register();

AnnotationRegistry::registerLoader(function($class) use ($loader) {
    $loader->loadClass($class);
    return class_exists($class, false);
});
AnnotationRegistry::registerFile(__DIR__.'/../src/vendor/doctrine/lib/Doctrine/ORM/Mapping/Driver/DoctrineAnnotations.php');

require_once __DIR__.'/../src/vendor/swiftmailer/lib/classes/Swift.php';
Swift::registerAutoload(__DIR__.'/../src/vendor/swiftmailer/lib/swift_init.php');
