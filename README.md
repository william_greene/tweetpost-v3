# Tweetpo.st - A smarter way to connect Twitter and Facebook by [Snowball Factory](http://snowballfactory.com/)

 * [Feature Overview](http://github.com/awesm/tweetpost-v3/wiki/Design)

 * [v2 Repository](http://github.com/awesm/tweetpost-v2)
 * [v3 Repository](http://github.com/awesm/tweetpost-v3)


## Configuration

### The project is configured with the following defaults

 * Twig is the only configured template engine;
 * Doctrine ORM/DBAL is configured;
 * Swiftmailer is configured;
 * Annotations for everything are enabled.
 * APC caching for everything is enabled.

### Check first that PHP is correctly configured for the CLI by running

 * `webapp/bin/check.php`

### If you want to use the CLI, a console application is available by running

 * `php webapp/tweetpost/console`


## Installation from Git (for development)

#### Setup instance in the cloud with [Amazon EC2](http://aws.amazon.com/) (preferred)

Sign up for [Amazon Web Services](http://aws.amazon.com/free/). Use the [Amazon Web Services Console](http://console.aws.amazon.com/) or install [AWS API tools](http://aws.amazon.com/developertools/351). An elaborate guide exists on [setting up an EC2 instance with AWS](http://paulstamatiou.com/how-to-getting-started-with-amazon-ec2).

 * [Introduction to Amazon EC2](http://docs.amazonwebservices.com/AWSEC2/2011-11-01/UserGuide/index.html)

 * [How to setup Amazon EC2 command line tools](http://docs.amazonwebservices.com/AWSEC2/2011-11-01/UserGuide/setting-up-your-tools.html)
 * [How to create an SSH key pair](http://docs.amazonwebservices.com/AWSEC2/2011-11-01/UserGuide/index.html?generating-a-keypair.html)
 * [How to add firewall rules to default security group for HTTP, HTTPS, MySQL, SSH](http://docs.amazonwebservices.com/AWSEC2/2011-11-01/UserGuide/index.html?adding-security-group-rules.html)
 * [How to add an elastic ip to an instance](http://docs.amazonwebservices.com/AWSEC2/2011-11-01/UserGuide/index.html?using-instance-addressing.html#using-instance-addressing-eips)

 * `ec2-run-instances ami-886e91e1 --instance-type m1.large --region us-east-1 --key ${EC2_KEYPAIR_US_EAST_1}`

 * `ssh ubuntu@server.aws.amazon.com`


#### Add DotDeb support for upgraded PHP/Nginx/MySQL/Redis

 * `sudo vim /etc/apt/sources.list.d/dotdeb.list`


        deb http://packages.dotdeb.org stable all
        deb-src http://packages.dotdeb.org stable all


#### Install [PHP](http://php.net) + [PECL](http://pecl.php.net/) extensions

 * `sudo apt-get install nginx-full php5-cli php5-fpm php5-apc php-pear php5 php5-cli php5-common php5-curl php5-dev php5-gd php5-imagick php5-intl php5-mcrypt php5-memcached php5-mysql php5-pspell php5-snmp php5-sqlite php5-suhosin php5-tidy php5-xmlrpc php5-xsl php5-redis mysql-server redis-server supervisor`
 * `sudo pear upgrade-all`

 * `sudo apt-get install build-essential openssl libssl-dev pkg-config libpcre3-dev libgearman-dev imagemagick libmagickwand-dev libssh2-1-dev libssh2-php libmemcached-dev shared-mime-info libmagic-dev libcurl4-openssl-dev libffi-dev libsqlite3-dev libpq-dev libreadline6-dev libreadline-dev wkhtmltopdf libxml2 libxml2-dev libxslt1.1 libxslt1-dev libssl0.9.8 libmysqlclient-dev`

 * `sudo pear config-set preferred_state beta`
 * `sudo pear config-set auto_discover 1`

 * `sudo pear update-channels`
 * `sudo pear upgrade PEAR`
 * `sudo pear upgrade-all`

 * `sudo pecl install -n apc pecl_http oauth memcached gearman igbinary`

 * `sudo pear install --alldeps pear.phpunit.de/DbUnit pear.phpunit.de/PHPUnit_Selenium pear.phpunit.de/PHPUnit_Story pear.phpunit.de/PHPUnit_TicketListener_GitHub pear.phpunit.de/PHP_Invoker pear.symfony-project.com/symfony`

#### Install Redis + Twig extensions from github

 * `git clone https://github.com/nicolasff/phpredis.git ; cd phpredis ; phpize ; ./configure ; make ; sudo make install ; cd .. ; rm -rf phpredis`
 * `git clone https://github.com/fabpot/Twig.git ; cd Twig/ext/twig ; phpize ; ./configure ; make ; sudo make install ; cd ../../../ ; rm -rf Twig`

#### Install development tools (optional)

 * `sudo apt-get install vim subversion git-core rsync acl s3cmd wget php5-xdebug ruby rubygems ruby1.9.1 ruby-full libmysql-ruby libopenssl-ruby libffi-ruby apache2-utils siege curl tinyca ant graphviz rrdtool doxygen nodejs npm mercurial python python-setuptools python2.7 python2.7-dev python-simplejson python-pip python-requests python-virtualenv exuberant-ctags screen jpegoptim optipng yui-compressor phpmyadmin phpsysinfo phpunit unoconv ffmpeg`

 * `sudo ln -s /usr/bin/ruby1.9.1 /usr/local/bin/ruby`
 * `sudo ln -s /usr/bin/gem1.9.1 /usr/local/bin/gem`
 * `sudo ln -s /usr/bin/irb1.9.1 /usr/local/bin/irb`

 * `sudo pecl upgrade`

 * `sudo gem update`
 * `sudo gem install capifony capistrano_rsync_with_remote_cache capistrano-ext less sass compass bundler`

### Clone git repository

 * `mkdir -p /var/www/tweetpo.st`

 * `git clone git@github.com:awesm/tweetpost-v3.git /var/www/tweetpo.st`
 * `git submodule update --init ; git submodule foreach 'git checkout master && git pull'`
 * `git pull --recurse-submodules`

### Configure Nginx + PHP + MySQL + Redis + Supervisord configuration

 * `sudo ln -s /var/www/tweetpo.st/current/webapp/tweetpost/config/nginx.conf /etc/nginx/sites-available/tweetpo.st`
 * `sudo ln -s /etc/nginx/sites-available/tweetpo.st /etc/nginx/sites-enabled/tweetpo.st`
 * `sudo ln -s /var/www/tweetpo.st/current/webapp/tweetpost/config/php.conf /etc/php5/conf.d/tweetpost.ini`
 * `sudo ln -s /var/www/tweetpo.st/current/webapp/tweetpost/config/supervisord.conf /etc/supervisor/conf.d/tweetpost.conf`

### Create database schema + import fixtures + install static assets

Don't run in production (as it creates the user and database):

 * `mysql -uroot -p < webapp/tweetpost/data/create_user_and_db.sql`

Update schema:

 * `php webapp/tweetpost/console doctrine:schema:update --force`
 * `php webapp/tweetpost/console doctrine:data:load`

Dump + optimize assets:

 * `php webapp/tweetpost/console assets:install webapp/web/static`
 * `php webapp/tweetpost/console assetic:dump`

Delete cache:

 * `sudo rm -rf webapp/tweetpost/cache/* webapp/tweetpost/logs/*`

For production:

 * `php webapp/tweetpost/console doctrine:schema:update --force --env=prod`
 * `php webapp/tweetpost/console assetic:dump --env=prod --no-debug`

### Restart services

 * `sudo service php5-fpm restart && sudo service nginx restart && sudo service mysql restart && sudo service redis-server restart && sudo service supervisor restart`

### Run the tweetpost engine

 * `sudo service supervisor stop`
 * `sudo service supervisor start`

Otherwise, you can manually start and stop the services:

 * `php webapp/tweetpost/console tweetpost:consumer --env=prod`
 * `php webapp/tweetpost/console tweetpost:publisher --env=prod`

### Maintenance tasks

Update connections validate/update tokens for users/apps/pages:

 * `php webapp/tweetpost/console tweetpost:cleanup:connections --env=prod`

Update facebook profiles/validate tokens:

 * `php webapp/tweetpost/console tweetpost:cleanup:users --env=prod`

Update twitter profiles/validate tokens:

 * `php webapp/tweetpost/console tweetpost:cleanup:twitter --env=prod`

Remove stale archived tweets:

 * `php webapp/tweetpost/console tweetpost:cleanup:tweets --env=prod`

### Promote a user to an admin

 * `php webapp/tweetpost/console fos:user:promote fbusername --env=prod`

### Tail log files

 * `tail -f /mnt/log/tweetpost/*`


## Tests

### Check that the tests pass by executing the test suite:

 * `phpunit -c webapp/tweetpost/phpunit.xml.dist --tap`


## License

### tweetpo.st is built using Symfony, Twig, Doctrine, Swiftmailer, PHPUnit, and Yahoo! User Interface libraries

 * [BSD License](http://creativecommons.org/licenses/BSD/)
