#!/bin/bash
cd /home/ubuntu/tweetpost-v3
php webapp/tweetpost/console assets:install webapp/web/static
php webapp/tweetpost/console assetic:dump --env=prod --no-debug
php webapp/tweetpost/console doctrine:schema:update --force --env=prod
sudo rm -rf /home/ubuntu/tweetpost-v3/webapp/tweetpost/cache/* /home/ubuntu/tweetpost-v3/webapp/tweetpost/logs/*
cd /var/www/tweetpo.st
TODAY=`date +%Y-%m-%d-%H-%M-%S`
DEPLOYDIR="/var/www/tweetpo.st/$TODAY"
cp -r /home/ubuntu/tweetpost-v3 $DEPLOYDIR
ln -s $DEPLOYDIR /var/www/tweetpo.st/current-deploy
sudo mv /var/www/tweetpo.st/current-deploy /var/www/tweetpo.st/current
sudo rm -rf /var/www/tweetpo.st/current/webapp/tweetpost/logs
sudo ln -s /mnt/log/tweetpost /var/www/tweetpo.st/current/webapp/tweetpost/logs
php current/webapp/tweetpost/console cache:warmup --env=prod --no-debug
sudo chmod -R 777 current/webapp/tweetpost/cache current/webapp/tweetpost/logs
sudo setfacl -R -m u:www-data:rwx current/webapp/tweetpost/cache current/webapp/tweetpost/logs
sudo setfacl -dR -m u:www-data:rwx current/webapp/tweetpost/cache current/webapp/tweetpost/logs
sudo setfacl -R -m u:ubuntu:rwx current/webapp/tweetpost/cache current/webapp/tweetpost/logs
sudo setfacl -dR -m u:ubuntu:rwx current/webapp/tweetpost/cache current/webapp/tweetpost/logs
sudo service nginx restart; sudo service php5-fpm restart; 
