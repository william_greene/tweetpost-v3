# [TweetPo.st](http://tweetpo.st/) v3 Summary

## Summary / Background
[TweetPo.st](http://tweetpo.st/) is a smarter way to update Facebook from Twitter. The current version of [TweetPo.st](http://tweetpo.st/) was built in Rails using EventMachine, which we think is more complicated than it has to be at this point because of the new Twitter and Facebook APIs that are available since the last version. Due to the complexity of the current architecture, we hit scaling issues at ~10,000 active users and [closed TweetPo.st to new signups about 9 months ago](http://blog.snowballfactory.com/2010/04/30/tweetpo-st-suspending-new-signups/). A lot of that complexity was in order to handle failure cases with Facebook's REST API (which had already been silently deprecated while they worked on the Graph API). We are hoping that the newer APIs will allow for a much simpler (and thus more scalable) approach which can allow us to open TweetPo.st back up to new users.

### Existing Functionality [[v2](https://github.com/awesm/tweetpost-v2)]

The basic workflow of [TweetPo.st](http://tweetpo.st/) is:

1. Get tweet from Twitter
1. Apply settings-based filter rules to decide if it should be posted to Facebook
1. If it should be posted to Facebook, apply settings-based transform rules to the tweet
1. Post to Facebook

To summarize ui/functionality:

* Content
 * Homepage
* User
 * Login with facebook connect
 * Logout
* Connections (post configurations)
  * Add Connections
     * Link twitter accounts via oauth
     * Link facebook accounts / pages
     * Select options for filtering
         * Integrate twitter api for converting twitter usernames to real names (facebook doesn't currently allow you to tag friends in posts via the API)
         * Integrate with Awesm API for rewriting urls
         * Filter based on hashtags
  * Edit connections
  * Delete connections
  * Toggle status (enable/disable) connections
* Tweetpost Engine
 * Consume twitter streaming api to fetch tweets in real-time for users
 * Consume twitter apis for private users (all public users should come from streaming api)
 * Publish updates to facebook profiles/pages based on post configurations for users
* Javascript Integrations
 * Facebook Connect
 * Google Analytics
 * User Voice

### New Functionality [[v3](https://github.com/awesm/tweetpost-v3)]
1. Completely rewrite the existing v2 functionality of tweetpost engine + webapp in PHP
  1. Migrate backend to use job queue (gearman) to scale out workers
  1. Refactor facebook/twitter integration to use latest (opengraph/streaming) apis
1. Switch from Twitter Streaming API with follow predicate to Twitter Site Streams API
1. Switch from publishing via FB REST API to FB Graph API
1. Improve Facebook preview generation logic
  1. Migrate to use pro.embed.ly for generating previews
1. Add Twitter app blacklist setting based on Tweet source (NB: Do NOT filter on the &lt;source&gt; value within <retweeted_status> if it's a native RT)
  1. UI improvements to manage blacklisting settings
  1. Never post anything with source=Facebook (e.g. https://twitter.com/#!/NOH8Campaign/statuses/70594089069981696)
1. Improve awe.sm integration
  1. use /url/retweet API
  1. add service_userid
  1. use /url/update API to pass service_postid after successful post
1. Deployment on amazon ec2 infrastructure
1. Handle deletion events in the Twitter Streaming API appropriately by not posting or deleting corresponding FB post
1. Add special case handling of native RTs (i.e. tweet events that contain a <retweeted_status> field in the Streaming API)
  1. UI improvements to add an option to never post native RTs (disabled by default)
  1. If a tweet has a &lt;retweeted_status&gt; node and the RT filtering setting isn't enabled, the content of the resulting FB post should use the following construction:
"&lt;retweeted_status&gt;&lt;text&gt; /via &lt;retweeted_status&gt;&lt;user&gt;&lt;name&gt;"


#### Preview [TweetPo.st Beta](http://beta.tweetpo.st/)
Add hostname for dns resolution by adding the following line to your /etc/hosts:
`23.20.3.18    tweetpo.st beta.tweetpo.st`
